.. include:: ../../replaces.txt
.. _concepts-sec:

************************
Defining and solving BVP
************************

Model problems
--------------

Poisson equation
^^^^^^^^^^^^^^^^

Helmholz equation
^^^^^^^^^^^^^^^^^

Linear elasticity equation
^^^^^^^^^^^^^^^^^^^^^^^^^^

Handling boundary conditions
----------------------------
Dirichlet conditions
^^^^^^^^^^^^^^^^^^^^

Neumann conditions
^^^^^^^^^^^^^^^^^^

Robin conditions
^^^^^^^^^^^^^^^^
