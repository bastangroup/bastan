# -*- coding: utf-8 -*-
"""Module with geometric tools.
"""

__authors__ = 'RP'
__copyright__ = 'Roman Putanowicz'
__date__ = '19/08/2020'

import numpy
import enum
from bastan import utils
from bastan import jsonsupport
from bastan.jsonsupport import JSONBased


class CSType(enum.IntEnum, metaclass=utils.enums.MetaIntEnum):
    """Enum coordinate system type 
    MetaIntEnum adds to this class two convenient methods: fromStr, convert
    """
    CARTESIAN = 0
    CYLINDRICAL = 1
    SPHERICAL = 2
    SPHERICAL_MATH = 2
    SPHERICAL_ISO = 3


def rectifyAngleMeasure(name):
    """Punt angle measure name in standard one of standard forms: radians, degrees.
    If name is not among any recognizible forms raise ValueError.
    Accepted forms are any capitalisation of : deg, degrees, rad, radians
    """
    name_ = name.lower()
    if name_ in ['degrees', 'deg']:
        return 'degrees'
    elif name_ in ['radians', 'rad']:
        return 'radians'
    else:
        raise ValueError(f"Invalid string for angle measure {name}")


class CoordSys(JSONBased):
    JSONkey = 'coordsys'
    JSONattributes = [
            ("cstype", CSType.fromStr),
            ("origin", jsonsupport.asparametricarray('float64')),
            ("angle_as", rectifyAngleMeasure)
                ]

    def __init__(self, cstype=CSType.CARTESIAN, angle_as='radians'):
        self.origin = numpy.array([0.0, 0.0, 0.0], dtype='float64')
        self.cstype = cstype
        self.angle_as = rectifyAngleMeasure(angle_as)

    def toCartesian(self, coords, alwayscopy=True):
        if self.cstype is CSType.CARTESIAN:
            if alwayscopy:
                return numpy.copy(coords)
            else:
                return coords
        new_coords = numpy.empty(coords.shape, dtype=coords.dtype)
        if self.angle_as == 'degrees':
            azimuth = numpy.radians(coords[:, 1])
        else:
            azimuth = coords[:, 1]
        if self.cstype is CSType.CYLINDRICAL:
            new_coords[:, 0] = coords[:, 0] * numpy.cos(azimuth)
            new_coords[:, 1] = coords[:, 0] * numpy.sin(azimuth)
            if coords.shape[1] > 2:
                new_coords[:, 2] = coords[:, 2]
        elif self.cstype is CSType.SPHERICAL:
            if self.angle_as == 'degrees':
                inclination = numpy.radians(coords[:, 2])
            else:
                inclination = coords[:, 2]
            si = numpy.sin(inclination)
            new_coords[:, 0] = coords[:, 2] * numpy.cos(azimuth) * si
            new_coords[:, 1] = coords[:, 2] * numpy.sin(azimuth) * si
            new_coords[:, 2] = coords[:, 2] * numpy.cos(inclination)
        else:
            raise NotImplementedError()
        return new_coords


