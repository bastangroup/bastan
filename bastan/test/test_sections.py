"""Test sections facilities 
"""

__authors__ = 'RP'
__copyright__ = '(c) 2020 BASTAN Group'
__date__ = '2020.08.04  9:8:43'
__version__ = '0.1.0'

import unittest
import json
import numpy
from bastan import structures 
from bastan import jsonsupport

DATA_CASE_1="""
{
  "sections" : {
    "column" : {
      "forBars" : [0,2],
      "area" : 0.1,
      "material" : "steelA"
    }
}
}
"""

class TestSections(unittest.TestCase):
    def test_setup_sections(self):
        data = json.loads(DATA_CASE_1)
        sc = structures.Sections()
        sc.setup(data['sections'])
        self.assertEqual(len(sc), 1)
        self.assertEqual(sc['column'].material, 'steelA')
