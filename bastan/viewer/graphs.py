"""Module providing utilities for making 2D graphs"""

import matplotlib
matplotlib.use('Qt5Agg')

from PySide2 import QtCore, QtWidgets
from PySide2.QtCore import Signal, Slot

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

from external.pyqtgraph.parametertree import ParameterTree, Parameter
import external.pyqtgraph as pyg


class GraphsWidget(pyg.PlotWidget):
    def __init__(self, parent=None, background='default', plotItem=None, **kargs):
        super().__init__(parent=parent, background=background, plotItem=plotItem, **kargs)
        self.legend = pyg.LegendItem((80, 60), offset=(70, 20))
        self.legend.setParentItem(self.graphicsItem())


class GraphsTree(ParameterTree):
    sigXDataChanged = Signal(str, object)  # self, name

    def __init__(self, simulator):
        super().__init__()
        self._simulator = simulator
        self._root = None
        self._xData = range(0)

    def setParameters(self, param, showTop=True):
        super().setParameters(param, showTop)
        self._root = param

    @Slot(str)
    def onXDataChanged(self, data_name):
        pass

    @property
    def xData(self):
        return self._xData

    def setGraphs(self, plotter):
        """Generate dictionary with top level management of graphs by parsing
        CSV file header
        """
        root = Parameter.create(name='params', type='group')
        return
        for header in sorted(self._simulator.headers[1:]):
            strsp = header.split(':')
            if len(strsp)  != 2:
                raise RuntimeError(f"Invalid CSV header: {header}")
            bodyname, param = strsp
            bodieslist = [ item.name() for item in root.children()]
            if bodyname not in bodieslist:
                bodyparam = Parameter.create(name=bodyname, type='group', expanded=False)
                root.addChild(bodyparam)
            else:
                bodyparam = root.child(bodyname)
            strsp = param.split('_')
            if len(strsp) > 1:
                propname, compname = strsp
            else:
                propname = param
                compname = None
            propnames = [item.name() for item in bodyparam.children()]
            if propname not in propnames:
                propparam = SimulationGraph(self._simulator, plotter, propname, header)
                bodyparam.addChild(propparam)
            else:
                propparam = bodyparam.child(propname)

            compnames = [item.name() for item in propparam.children()]
            if compname is not None and compname not in compnames:
                compparam = SimulationGraph(self._simulator, plotter, compname, header)
                propparam.addComponent(compparam)
                self.sigXDataChanged.connect(compparam.onXDataChanged)
        self.setParameters(root)


class SimulationGraph(Parameter):
    default_params = [{'name': 'Show', 'type': 'bool', 'value': False, 'tip': "Show graph"},
                      {'name': 'Color', 'type': 'color', 'value': "FF0", 'tip': "Graph color"},
                      {'name': 'Width', 'type': 'float', 'value': 1.0, 'tip': 'Line width'}
                      ]

    def __init__(self, simulator, plotter, name, header):
        super().__init__(name=name, type='group', expanded=False,
                         children=SimulationGraph.default_params)
        self._simulator = simulator
        self._header = header
        self._setup_signals()
        self._plotter = plotter
        self._curve = None
        self._components = []

    def addComponent(self, parameter):
        self.addChild(parameter)
        self._components.append(parameter)

    def _setup_signals(self):
        self.child('Show').sigValueChanged.connect(self.onShow)
        self.child('Color').sigValueChanged.connect(self.onSetPen)
        self.child('Width').sigValueChanged.connect(self.onSetPen)

    def _make_curve(self):
        if self._curve is None:
            self._curve = self._plotter.plot()
            y = self._simulator.getColumn(self.header)
            x = range(1,len(y)+1)
            self._curve.setData(x, y)
        else:
            self._plotter.addItem(self._curve)

    @property
    def header(self):
        return self._header

    @Slot()
    def onSetPen(self, value):
        if self._curve is not None:
            width = self.child('Width').value()
            color = self.child('Color').value()
            pen = pyg.mkPen(color=color, width=width)
            self._curve.setPen(pen)

    @Slot()
    def onXDataChanged(self, dataname, xdata):
        if xdata is None:
            raise RuntimeError("Invalid none data")
        if self._curve is not None:
            y = self._simulator.getColumn(self.header)
            self._curve.setData(xdata, y)

    @Slot()
    def onShow(self, value):
        flag = self.child('Show').value()
        if self._components:
            self.showComponentCurves(flag)
        else:
            self.showCurve(flag)

    def setVisible(self, flag):
        self.child('Show').setValue(flag)

    def showCurve(self, flag):
        if flag:
            self._make_curve()
            self._plotter.legend.addItem(self._curve, self.header)
        else:
            if self._curve is not None:
                self._plotter.removeItem(self._curve)
                self._plotter.legend.removeItem(self._curve)

    def showComponentCurves(self, flag):
        for component in self._components:
            component.setVisible(flag)



class MplCanvas(FigureCanvasQTAgg):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)
