# -*- coding: utf-8 -*-
"""
This module provides very generic model for Structure 
"""

class Structure(object):
    _attrib_datakey = {'geom' : 'bargeom',
                       'materials' : 'materials',
                       'loads' : 'loads',
                       'supports' : 'supports'}
    def __init__(self):
        self.geom = None
        self.loads = None
        self.supports = None
        self.materials = None

    def setup(self, data):
        """Setup structure by calling setup for all its attributes"""
        for attname, key in Structure._attrib_datakey.items():
            attr = getattr(self, attname)
            if attr is None:
                raise RuntimeError(f"Attribute f{attname} not set")
            attr.setup(data.get(key))
