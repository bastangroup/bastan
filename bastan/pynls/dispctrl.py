import numpy

from bastan.pynls import unisch
from bastan.pynls import ctrl
from bastan.pynls.ctrl import ControlType


class DisplacementControl(unisch.UnifiedSchemes):
    def __init__(self, model, control, linsys):
        super().__init__(model, control, linsys)
        self._iCtrlEq = control.ctrlEq
        self._dCtrlFactor = control.ctrlFactor

        n = self.NumEq
        self._pdL = None
        self._pdXPrv = None
        if self.ctrlType == ctrl.ControlType.VARIABLE:
            self._pdL = numpy.zeros(n, dtype='float64')
            self.pdXPrv = numpy.zeros(n, dtype='float64')

    def Lambda(self):
        if self._iCurrIte == 1:
            if self._iCurrStep != 1 and self.ctrlType == ControlType.VARIABLE:
                self._pdL += numpy.fabs(self._pdX - self._pdXPrv)
                numpy.copyto(self._pdXPr, self._pdX)

            # Find degree of freedom with the largest displacement
            eqMax = 0
            valMax = numpy.fabs(self._pdDx1[eqMax])
            for i in range(self.NumEq):
                valCurr = numpy.fabs(self._pdDx1[i])
                if valCurr > valMax:
                    eqMax = i
                    valMax = valCurr

            # if the control DOF is not the one with the max displacement, switch the control dof
            # also keep the sign of the max dof
            if eqMax != self._iCtrlEq:
                direction = 1
                if self._pdDx1[eqMax] * self._pdDx1[self._iCtrlEq] <= 0.0:
                    direction = -1
                self._dCtrlFactor *= direction * self._pdL[eqMax] / self._pdL[self._iCtrlEq]
                self._iCtrlEq = eqMax
            self._dLambda = self._dCtrlFactor / self._pdDx1[self._iCtrlEq]
        else:
            self._dLambda = -self._pdDx2[self._iCtrlEq] / self._pdDx1[self._iCtrlEq]
