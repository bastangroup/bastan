# -*- coding: utf-8 -*-
"""
Support module for json based classes
"""

import collections
import json
import math
import numpy
import os
from simpleeval import simple_eval
import simpleeval

simpleeval.DEFAULT_FUNCTIONS.update(sqrt=lambda x:math.sqrt(x))

PARAMS = {}

def ZerosMatrix(row, col):
    return numpy.zeros((row, col), dtype='float64')


def ZerosVector(nelem):
    return numpy.zeros((nelem,), dtype='float64')


def OnesVector(nelem):
    return numpy.ones((nelem,), dtype='float64')


def nochange(arg):
    return arg

def fromparam(arg):
    return simple_eval(str(arg), names=PARAMS)

class asarray(object):
    def __init__(self, dtype='float64'):
        self.dtype = dtype

    def __call__(self, arg):
        return numpy.asarray(arg, dtype=self.dtype)

def setupParams(data):
    params = data.get('params')
    if params is not None:
        PARAMS.update(params)

def setParams(**kwarg):
    PARAMS.update(kwarg)


class asparametricarray(object):
    @staticmethod
    def subst(txt):
        val = PARAMS.get(txt)
        if val is None:
            val = txt
        return str(val)

    def __init__(self, dtype='float64'):
        self.dtype = dtype

    def __call__(self, arg):
        val = [fromparam(r) for r in arg]
        return numpy.array(val, dtype=self.dtype)


def asranges(arg):
    xranges = [(lambda x: range(x[0], x[-1]+1))([l for l in map(int, str(r).split('-'))]) for r in arg]
    return numpy.asarray([y for x in xranges for y in x], dtype='uint')


def make_unique(key, dct):
    counter = 0
    unique_key = key
    while unique_key in dct:
        counter += 1
        unique_key = '{}_{}'.format(key, counter)
    return unique_key


def parse_object_pairs(pairs):
    dct = collections.OrderedDict()
    for key, value in pairs:
        if key in dct:
            key = make_unique(key, dct)
        dct[key] = value
    return dct


class JSONBased(object):
    @classmethod
    def fromData(cls, data, params=None, key=None):
        if params is not None:
            PARAMS.update(params)
        self = cls()
        if key is None:
            key = cls.JSONkey
        if key is not "":
            data = data.get(key)
        if data:
            self.setup(data)
        return self

    @classmethod
    def fromJSON(cls, path):
        print(cls)
        self = cls()
        with open(path) as fhandle:
            data = json.load(fhandle, object_pairs_hook=parse_object_pairs)
            setupParams(data)
            return self.fromData(data)

    def setup(self,  data, params=None):
        if data is None:
            print("WARNING:", f"None pased to setup of {type(self).__name__}")
            return
        if params is not None:
            PARAMS.update(params)
        for name, decoder in self.JSONattributes:
            val = decoder(data.get(name, getattr(self, name)))
            setattr(self, name, val)

    def setupCompound(self, data, attrName, compoundClass, params=None, optional=False):
        if data is None:
            print("WARNING:", f"None pased to setup of {type(self).__name__}")
            return
        if params is not None:
            PARAMS.update(params)
        compoundData = data.get(attrName)
        if compoundData is None:
            if optional:
                return
            raise RuntimeError(f"No data for {attrName} compound")
        attr = getattr(self, attrName)
        for name, itemdata in compoundData.items():
            item = attr.get(name)
            if item is not None:
                raise RuntimeError(f"Item {name} in {attrName} already defined")
            item = compoundClass(name)
            item.setup(itemdata)
            attr[name] = item

    def __init__(self):
        pass


class JSONBasedCollection(collections.abc.Mapping, JSONBased):
    def __init__(self):
        self._collection = {}

    def __len__(self):
        return len(self._collection)

    def __iter__(self):
        return self._collection.__iter__()

    def __getitem__(self, key):
        return self._collection.get(key)

    def setup(self, data, params=None):
        super().setup(data, params=params)
        if data is None:
            print("WARNING:", f"None pased to setup of {type(self).__name__}")
            return
        for name, itemdata in data.items():
            item = self.get(name)
            if item is not None:
                raise RuntimeError(f"type(self).__name__ {name} already defined")
            item = self.__class__.ItemClass(name)
            item.setup(itemdata)
            self._collection[name] = item


class PolyVal(JSONBased):
    JSONkey = "PolyVal"
    JSONattributes = [
        ("values", asparametricarray('float64')),
        ("ksi", asparametricarray('float64'))
        ]

    def __init__(self):
        super().__init__()
        self.values = []
        self.ksi = []

    def setup(self, data, params=None):
        super().setup(data, params=params)
        if len(self.ksi) == 0:
            self.ksi = numpy.array([0.0, 1.0], dtype='float64')
        if len(self.ksi) == 1:
            raise ValueError(f"Invalid number of ksi values: {self.ksi}")
        if len(self.values) == 1:
            self.values = numpy.full(self.ksi.shape, self.values[0])
        if len(self.values) < 2:
            raise ValueError(f"Not enough values for PolyVal")
        if len(self.values) != len(self.ksi):
            raise ValueError(f"#values != #ksi :  {self.ksi}, {self.values}")
