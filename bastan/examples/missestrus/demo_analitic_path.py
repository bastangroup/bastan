# -*- coding: utf-8 -*-
"""
Plotting analitic equilibrium path
==================================

"""

__authors__ = 'RP'
__copyright__ = '(c) 2021 BASTAN Group'
__date__ =  "Tue Apr  6 09:58:57 CEDT 2021"
__version__ = '0.1.0'

import numpy as np
import os
import sys

try:
    os.environ["BASTAN_DIR"]
except KeyError:
    print("Please set the environment variable BASTAN_DIR")
    sys.exit(1)

sys.path.append(os.environ['BASTAN_DIR'])

import bastan
import json

PKGDIR = os.path.dirname(bastan.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')

class DemoMissesAnaliticPath(bastan.examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        plotter = bastan.structures.MissesPlotter()
        vmt = bastan.structures.MissesTruss(L=1.0, H=0.2, E=1, A=1, P=1)
        plotter.plot_analitic_equilibrium_path(vmt)
        for i in 0, 1:
            lpt = vmt.get_limit_point(i)
            yoffset = 10 - i*10
            plotter.ax.annotate("limit point", xy=lpt, xytext=(10,yoffset), textcoords='offset pixels')
            plotter.ax.plot(*lpt, "ro")
        plotter.show()

if __name__ == '__main__':
    demo = DemoMissesAnaliticPath()
    demo.run()
