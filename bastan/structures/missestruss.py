# -*- coding: utf-8 -*-
"""
This module defines MissesTruss class with full analytical and FEM description
of von Misses truss.
"""

import numpy
import math
import json
import inspect

from bastan import structures
import bastan.jsonsupport
from bastan.jsonsupport import JSONBased

import matplotlib.pyplot as plt

VON_MISSES_TRUSS_DATA = """
{
  "params" : {
    "L" : 1.0,
    "H" : 1.0,
    "Px" : 0.0,
    "Pz" : -1.0,
    "E" : 1.0,
    "A" : 1.0
  },
  "Truss" : {
    "bargeom" : {
      "dim" : 3,
      "coords" : [
        "-L", 0.0, 0.0,
        0.0, 0.0, "H",
        "L", 0.0, 0.0],
      "elems" : [0,1,
                 1,2],
      "sections" : {
        "sec_all" : {
          "forBars" : ["0-1"],
          "area" : "A",
          "material" : "mat_all"
        }
      }
    },
    "materials" : {
      "mat_all" : {
        "E" : "Young" 
      }
    },
    "supports" : {
      "pin" : {
        "Tx" : "Fixed",
        "Tz" : "Fixed",
        "atNodes" : [0,2]
      },
      "inplane" : {
        "Ty" : "Fixed",
        "atNodes" : ["0-2"]
      }
    },
    "loads" : {
      "cases" : {
        "live_1" : {
          "nodal" : {
            "Force" : {
              "Fz" : "Pz",
              "Fx" : "Px",
              "atNodes" : [1]
            }
          }
        }
      }
    }
  }
}
"""

class MissesTruss(structures.Truss):
    """
    Parameters
    ----------
    L : `double`
        Half of the span length
    H : `double`
        Truss height
    A : `double`
        Bar's corss-section area
    E : `double`
        Young modulus
    q : `double`
        Truss deflection
    """
    JSONkey = 'Truss'
    JSONattributes = {}
    def __init__(self, L=1.0, H=1.0, E=1.0, A=1.0, P=1.0):
        super().__init__()
        data = json.loads(VON_MISSES_TRUSS_DATA)
        bastan.jsonsupport.setupParams(data)
        self._L = L
        self._H = H
        self._E = E
        self._A = A
        self._P = P
        self._initKT = self.EA/self.lzero * self.sinbetazero**2
        self._scaledP = self._P / (self.EA * self.sinbetazero**3)
        self._initR = self.EA * self.sinbetazero**3
        self.q = 0.0
        params = dict(zip(('L', 'H', 'E', 'A'), (L, H, E, A)))
        self.setup(data["Truss"], params)

    def __str__(self):
        """
        Generate description of the structure
        """
        str = ""
        str += 'von Misses truss summary:\n'
        str += '-------------------------\n'
        return str
        print(f"Half length: {self.L}")
        print(f"Height : {self.H}")
        print(f"Area : {self.A}")
        print(f"Young modulus: {self.E}")
        for i, b in enumerate(self.geom.bars):
            print(f"Bar {i} length {b.length}")

    @property
    def E(self):
        return self._E

    @property
    def A(self):
        return self._A

    @property
    def L(self):
        return self._L

    @property
    def H(self):
        return self._H


    @property
    def lzero(self):
        """Initial length of bars"""
        return math.sqrt(self.L**2 + self.H**2)

    @property
    def sinbetazero(self):
        """Sinus of truss slope angle in initial configuration"""
        return self.H/self.lzero

    @property
    def EA(self):
        """Bar stiffness"""
        return self.E*self.A

    def eta(self, q):
        """Dimensionless truss deflection"""
        return q/self.H

    def KT(self, q):
        """Tangent stiffness"""
        e = self.eta(q)
        return  (3.0 * e**2 - 6.0 * e + 2.0)
        return self._initKT * (3.0 * e**2 - 6.0 * e + 2.0)

    def residuum(self, q):
        """Equilibrium equation residuum"""
        e = self.eta(q)
        return self._initR * (-(e**3 - 3*e**2 + 2*e) + self._scaledP)

    def scaled_force(self, q):
        e = self.eta(q)
        return e**3 - 3*e**2 + 2*e

    def get_limit_point(self, id):
        """Return coordinates of limit point on scaled equilibrium path"""
        s3 = math.sqrt(3)
        if id == 0:
            return 1-1/s3, 2/(3*s3)
        elif id == 1:
            return 1+1/s3, -2/(3*s3)
        else:
            raise AttributeError("Limit point id should be 0 or 1")

    def solve_equilibrium(self, control, epsR, epsq):
        N = len(control)
        q = numpy.zeros((N+1,), dtype='float64')
        eta = numpy.zeros((N+1,), dtype='float64')
        lastInc = numpy.zeros((N+1,), dtype='float64')
        iterCount = numpy.zeros((N+1,), dtype='int')
        control = numpy.array(control) * (self.sinbetazero**3 * self.EA)
        for m in range(N):
            dP = control[m]
            dq = 0.0
            ddq = dP / self.KT(q[m])
            ddq_0 = ddq
            R_0 =  self.residuum(q[m])
            dq += ddq
            print("ddq = ", ddq)
            while True:
                R_i =  self.residuum(q[m]+dq)
                ddq_next = R_i / self.KT(q[m]+dq)
                print('m : q, KT, Residuum, ddq_next: ', m, q[m]+dq, self.KT(q[m]+dq), R_i, ddq_next)
                errorR = abs(R_i/R_0)
                errorq = abs(ddq_next/ddq_0)
                dq += ddq_next
                iterCount[m] += 1
                lastInc[m] = ddq_next
                print('Error : ', errorR, errorq)
                if errorR < epsR and errorq < epsq:
                    break
                if iterCount[m] >= 4:
                    break
            print("After iteration ", dq, self.eta(dq))
            q[m+1] = q[m] + dq
        q = numpy.array(list(map(self.eta, q)))
        lastInc = numpy.array(list(map(self.eta, lastInc)))
        return q[1:], lastInc, iterCount

class MissesPlotter:
    def __init__(self):
        self.fig, self.ax = plt.subplots()

    def plot_analitic_equilibrium_path(self, vmt, etamax=2.2):
        e = numpy.linspace(0, etamax, dtype='float64')
        q = e * vmt.H
        pscaled = numpy.array(list(map(vmt.scaled_force, q)))
        self.ax.plot(e, pscaled, label='Scaled force')
        self.ax.legend()
        self.ax.set_ylabel(r'$P/(EA\sin^3(\beta_0)$')
        self.ax.set_xlabel(r'$q/H$')
        self.ax.grid()

    def plot_iteration_step_load_control(self, vmt, q, deltap):
        p = vmt.scaled_force(q)
        kt = vmt.KT(q)
        dq = deltap/vmt._scaledP/kt
        print('ddq : ', dq)
        pdq = vmt.scaled_force(q+dq)
        x = [q, q+dq, q+dq]
        x = numpy.array(list(map(vmt.eta, x)))
        y = [p, p+deltap, pdq]
        self.ax.plot(x, y, '--')
        return q+dq, pdq

    def show(self):
        plt.show()


