import numpy
from PySide2.Qt3DExtras import Qt3DExtras
from PySide2.QtCore import Property, Signal, QByteArray
from PySide2.Qt3DCore import Qt3DCore
from PySide2.Qt3DRender import Qt3DRender

class LineGeometry(Qt3DRender.QGeometry):
    def __init__(self, start, end, color='Black', parent=None):
        Qt3DRender.QGeometry.__init__(self, parent)

        coords = numpy.array([start[0],end[0],start[1],end[1], start[2], end[2]], dtype='float32')
        coords = numpy.array([start[0],start[1], start[2], end[0],end[1],end[2]], dtype='float32')

        self.buf = Qt3DRender.QBuffer()
        self.buf.setData(coords.tobytes())

        self.positionAttribute = Qt3DRender.QAttribute(self)
        self.positionAttribute.setName(Qt3DRender.QAttribute.defaultPositionAttributeName())
        self.positionAttribute.setVertexBaseType(Qt3DRender.QAttribute.Float)
        self.positionAttribute.setVertexSize(3);
        self.positionAttribute.setAttributeType(Qt3DRender.QAttribute.VertexAttribute)
        self.positionAttribute.setBuffer(self.buf)
        #positionAttribute.setByteStride(3 * sizeof(float));
        self.positionAttribute.setCount(2)
        self.addAttribute(self.positionAttribute)

        indexBytes = numpy.array([0,1], dtype='uint')

        self.indexBuffer = Qt3DRender.QBuffer()
        self.indexBuffer.setData(indexBytes.tobytes())

        self.indexAttribute = Qt3DRender.QAttribute(self)
        self.indexAttribute.setVertexBaseType(Qt3DRender.QAttribute.UnsignedInt)
        self.indexAttribute.setAttributeType(Qt3DRender.QAttribute.IndexAttribute)
        self.indexAttribute.setBuffer(self.indexBuffer)
        self.indexAttribute.setCount(2)
        self.addAttribute(self.indexAttribute)

        self.line = Qt3DRender.QGeometryRenderer(parent)
        self.line.setGeometry(self)
        self.line.setPrimitiveType(Qt3DRender.QGeometryRenderer.Lines)
        material = Qt3DExtras.QPhongMaterial(parent)
        material.setAmbient(color)
        self.lineEntity = Qt3DCore.QEntity(parent)
        self.lineEntity.addComponent(self.line)
        self.lineEntity.addComponent(material)


class CoordSys(object):
    def __init__(self, parent=None, lengths=(1.0, 1.0, 1.0), symmetric=False):
        colors = ('Red', 'Yellow', 'Green')
        self.axes = []
        for i in range(3):
            pt = numpy.array([0.0,0.0,0.0])
            pt[i] = lengths[i]
            self.axes.append(LineGeometry(-pt*symmetric,pt, color=colors[i], parent=parent))
    
    def setVisible(self, flag):
        for axis in self.axes:
            axis.lineEntity.setEnabled(flag)
