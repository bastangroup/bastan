import numpy

class LinSys:
    def __init__(self):
        self._numEq = 0
        self._M = None

    @property
    def numEq(self):
        return self._numEq

    def Init(self, model):
        self._numEq = model.NumEq
        N = self.numEq
        self._M = numpy.zeros((N, N), dtype='float64')

    def AddA(self, i, j, val):
        self._M[i,j] = val

    def Solve(self, rhs, x):
        numpy.copyto(x, numpy.linalg.solve(self._M, rhs))
