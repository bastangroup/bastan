# -*- coding: utf-8 -*-
"""
Module providing data structures describing supports
"""

import numpy
import math

from bastan import jsonsupport
from bastan.jsonsupport import JSONBased, JSONBasedCollection

# Dictionary of DOF names and their local indices
DOFNAMES = {n:i for i,n in enumerate(['Tx', 'Ty', 'Tz', 'Rx', 'Ry', 'Rz'])}

class Support(JSONBased):
    """Holds point support data
    """
    Keys = {'Free' : 'NaN',
            'Fixed' : 0.0
           }
    JSONattributes = [
            ("dofs", jsonsupport.nochange),
            ("atNodes", jsonsupport.asranges)
                ]
    counter = 0
    @staticmethod
    def getName(name):
        if name is None:
            Support.counter += 1
            name = f"support_{counter}"
        return name

    @staticmethod
    def mapKey(key):
        val = Support.Keys.get(key, key)
        return str(val)


    def __init__(self, name=None):
        self.dofs = numpy.full(len(DOFNAMES,), numpy.NaN, dtype='float64')
        self.atNodes = []
        self.name = Support.getName(name)

    def setup(self, data):
        super().setup(data)
        s = ','.join(Support.mapKey(x) for x in self.dofs) 
        self.dofs = numpy.fromstring(s, sep=',',dtype='float64')
        for item, idx in DOFNAMES.items():
            if item in data:
                self.dofs[idx] = Support.mapKey(data[item])

    def _isFixed(self, dofName, idx, names=None):
        hasIt = names is None or dofName in names
        isFixed = not numpy.isnan(self.dofs[idx])
        return  hasIt and isFixed 

    def countFixedDofs(self, names=None):
        """Return number of DOFs that have fixed value"""
        return sum(1 for dn, idx in DOFNAMES.items()
                        if self._isFixed(dn, idx, names)) 

    def getFixedDofs(self, names=None):
        """Return dictionary {idx, val} of dof index and value if value
        is set"""
        return {idx:self.dofs[idx] for dn, idx in DOFNAMES.items()
                    if self._isFixed(dn, idx, names)}            
    

class Supports(JSONBasedCollection):
    JSONkey = 'supports'
    JSONattributes = []
    ItemClass = Support 

    def __init__(self):
        super().__init__()

    def countFixedDofs(self, names=None):
        """Return number of DOFs that have fixed value"""
        return sum(map(lambda sp: len(sp.atNodes)*sp.countFixedDofs(names), self.values()))
