export BASTAN_CONFIG_DIR

list-default :
	$(foreach prog, $(DEMOS), $(info $(prog)))

demo_% : demo_%.py
	python -O $< --close-images
