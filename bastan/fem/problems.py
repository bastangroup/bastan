# -*- coding: utf-8 -*-
"""
Module provide classes describing specific problems like: statics, stability,
dynamics, etc.

"""

import abc
import argparse
import os
import timeit
import numpy
import calfem.core as cfc
import scipy.linalg

from bastan import structures


class Workspace(object):
    def __init__(self):
        self.dofsPerNode = 0
        self.nnodes = 0
        self.elems = None
        self.edofs = None
        self.dofMap = {}
        self.loadMap = {}
        self.bcDofs = None
        self.bcVals = None
        self.arrays = {}

    @property
    def dofsPerElem(self):
        return self.nodesPerElem*self.dofsPerNode

    @property
    def nodesPerElem(self):
        return self.elems.shape[1]

    @property
    def ndofs(self):
        return self.nnodes * self.dofsPerNode

    @property
    def nelems(self):
        return self.elems.shape[0]

    def l2g(self, nodeIdx, ldof):
        """Map local dof at node to global one"""
        return nodeIdx*self.dofsPerNode + ldof

    def setupMap(self, kind, names):
        """Helper to setup dofMap or loadMap mappings

        Parameters
        ----------
            kind : one of 'dof', 'load'
            names : list of names of dofs or dof loads
        """
        attname = kind+'Map'
        namemap = {dn: idx for idx, dn in enumerate(names)}
        setattr(self, attname, namemap)

    def byDOF(self, name, atNode, within):
        locdof = self.dofMap[name]
        globdof = self.l2g(atNode, locdof)
        V = self.get(within)
        return V[globdof]

    def update(self, **kwargs):
        self.arrays.update(kwargs)

    def _get(self, name):
        """Return named array from workspace"""
        arr = self.arrays.get(name)
        if arr is None:
            raise RuntimeError(f"No array: {name} in workspace")
        return arr

    def get(self, *names):
        if len(names) == 1:
            return self._get(names[0])
        else:
            return (self._get(nm) for nm in names)

    def set(self, name, shape, dtype, fill=None, overwrite=False):
        if overwrite and self.arrays.get(name) is not None:
            raise RuntimeError(f"Setting array {name} will overwrite existing one")
        if fill is None:
            self.arrays[name] = numpy.empty(shape, dtype=dtype)
        else:
            self.arrays[name] = numpy.full(shape, fill, dtype=dtype)

    def setElemArrays(self, **kwargs):
        for name, cols in kwargs.items():
            shape = (self.nelems, cols)
            self.set(name=name, shape=shape, dtype='float64', fill=0.0)

    def setGlobalArrays(self, **kwargs):
        for name, dim in kwargs.items():
            shape = dim*(self.ndofs,)
            self.set(name=name, shape=shape, dtype='float64', fill=0.0)


class Problem(abc.ABC):
    """Abstract base class for problems
    """

    @abc.abstractmethod
    def solve(self, structure, *args, **kwargs):
        """Override this method with function to solve a problem"""
        ...

    @classmethod
    def derived(cls):
        return [c.__name__ for c in cls.__subclasses__()]

    def __init__(self, name=None, silent=False):
        self.name = name
        if self.name is None:
            self.name = type(self).__name__
        self.silent = silent
        self._start_time = 0.0
        self._end_time = 0.0
        self.args = None
        self.outdir = '.'
        self.parser = argparse.ArgumentParser(description="BASTAN Problem:")
        self._setup_parser()
        self.workspace = Workspace()
        self.fem = None

    def _setup_parser(self):
        """Connfigure parser arguments
        """
        self.parser.add_argument('--silent', action='store_true', dest='silent',
                                 help='If True print severla messages')

    def report(self, message):
        if not self.silent:
            print(message)

    def atStart(self):
        self._start_time = timeit.default_timer()
        self._end_time = self._start_time
        self.report('---------------------START PROBLEM------------------------')
        self.report(f"Running: {self.name}")

    def atEnd(self):
        self._end_time = timeit.default_timer()
        self.report('---------------------END of PROBLEM------------------------')
        self.printTiming()

    def printTiming(self):
        """Print information about problem execution time"""
        total_time = self._end_time - self._start_time
        self.report(f"{self.name} executed in {total_time:.{3}} seconds")

    def run(self, structure):
        """Run problem solver with some initialisation and finalization"""
        self.args, other = self.parser.parse_known_args()
        self.silent = self.args.silent
        self.atStart()
        self.solve(structure)
        self.atEnd()

    def _prepare_workspace(self, structure):
        self.fem = femFactory(structure)
        self.fem.prepareWorkspace(structure, self.workspace)

    def _assemble_elements(self, structure):
        for elemIdx in range(self.workspace.nelems):
            matrices = self.fem.calcElemMatrices(structure, elemIdx)
            edofs = self.workspace.edofs[elemIdx, :]
            for name, mtx in matrices.items():
                cfc.assem(edofs, self.workspace.get(name), mtx)

    def _assemble_loads(self, structure, caseName):
        self._assemble_nodal_loads(structure, caseName)
        self._assemble_distributed_loads(structure, caseName)

    def _assemble_distributed_loads(self, structure, caseName):
        pass

    def _assemble_nodal_loads(self, structure, caseName=None):
        if caseName is None:
            caseName = list(structure.loads.cases)[0]
        F = self.workspace.get('F')
        case = structure.loads.cases[caseName]
        for nodalLoad in case.nodal.values():
            for loadName, localdof in self.workspace.loadMap.items():
                idx = structures.NodalLoad.LOADNAMES.index(loadName)
                val = nodalLoad.loads[idx]
                for node in nodalLoad.atNodes:
                    globdof = int(self.workspace.l2g(node, localdof))
                    F[globdof] += val

    def _assemble_supports(self, structure):
        dofNames = self.workspace.dofMap.keys()
        # count how many restricted dofs
        nsupdofs = structure.supports.countFixedDofs(names=dofNames)
        bcDofs = numpy.zeros((nsupdofs,), dtype='uint')
        bcVals = numpy.zeros((nsupdofs,), dtype='float64')
        i = 0
        for supp in structure.supports.values():
            fixedDofs = supp.getFixedDofs(names=dofNames)
            for node in supp.atNodes:
                for localdof, dofval in fixedDofs.items():
                    bcDofs[i] = self.workspace.l2g(node, localdof)
                    bcVals[i] = dofval
                    i += 1
        self.workspace.bcDofs = bcDofs
        self.workspace.bcVals = bcVals

    def _solve_linear_system(self, structure):
        ws = self.workspace
        K, F = ws.get('K', 'F')
        d, R = cfc.solveq(K, F, self.workspace.bcDofs, self.workspace.bcVals)
        ws.update(d=d, R=R)

    def _postprocess_elements(self, structure):
        self.fem.gatherInternalForces(structure, self.workspace)


class Truss3D(object):
    """The purpose of this class is to make FEM model of a truss tructure"""

    DOFNAMES = ['Tx', 'Ty', 'Tz']
    LOADNAMES = ['Fx', 'Fy', 'Fz']

    KsigmaBase = numpy.array([[1, 0, 0, -1, 0, 0],
                              [0, 1, 0, 0, -1, 0],
                              [0, 0, 1, 0, 0, -1],
                              [-1, 0, 0, 1, 0, 0],
                              [0, -1, 0, 0, 1, 0],
                              [0, 0, -1, 0, 0, 1]], dtype='float64')

    def __init__(self):
        pass

    def prepareWorkspace(self, structure, workspace):
        assert structure.geom.dim == 3, f"Invalid structure dim {structure.geom.dim}"
        ws = workspace
        geom = structure.geom
        ws.dim = geom.dim
        ws.dofsPerNode = geom.dim
        ws.nnodes = geom.nnodes
        ws.elems = geom.elems
        ws.setupMap('dof', self.__class__.DOFNAMES[0:ws.dofsPerNode])
        ws.setupMap('load', self.__class__.LOADNAMES[0:ws.dofsPerNode])

        nodeLocDofs = numpy.arange(ws.dofsPerNode, dtype='uint')
        localDofs = numpy.tile(nodeLocDofs, ws.nodesPerElem)
        ws.edofs = numpy.repeat(ws.elems*ws.dofsPerNode, ws.dofsPerNode, axis=1)+localDofs
        ws.setGlobalArrays(d=1, R=1, F=1, K=2)


    def gatherInternalForces(self, structure, workspace):
        """Calculate and store internal forces in elements"""
        workspace.setElemArrays(N=1)
        for bar in structure.geom.bars:
            E = structure.materials[bar.section.material].E
            A = bar.section.area
            ep = [E, A]
            d, N = workspace.get('d', 'N')
            ed = d[workspace.edofs[bar.idx]]
            N[bar.idx] = cfc.bar3s(*bar.exyz, ep, ed)

    def calcElemMatrices(self, structure, barIdx):
        return {'K': self.calcLinearStiffnessMatrix(structure, barIdx)}

    def calcLinearStiffnessMatrix(self, structure, barIdx):
        bar = structure.geom.bars[barIdx]
        E = structure.materials[bar.section.material].E
        A = bar.section.area
        ep = [E, A]
        return cfc.bar3e(*bar.exyz, ep)

    def calcInitialStressMatrix(self, structure, barIdx, workspace):
        bar = structure.geom.bars[barIdx]
        N = workspace.get('N')[barIdx]
        L = bar.length
        C = bar.orientation
        T = numpy.kron(numpy.eye(2), bar.orientation)
        Ksigma = N/L * self.__class__.KsigmaBase
        return T.T @ Ksigma @ T


class Frame2D(object):
    """The purpose of this class is to make FEM model of a truss tructure"""

    DOFNAMES = ['Tx', 'Ty', 'Rz']
    LOADNAMES = ['Fx', 'Fy', 'Mz']
    KsigmaBase = numpy.array([[0, 0,    0, 0,   0,  0],
                              [0, 36,   3, 0, -36,  3],
                              [0, 3,    4, 0,  -3, -1],
                              [0, 0,    0, 0,   0,  0],
                              [0, -36, -3, 0,  36, -3],
                              [0, 3,   -1, 0,  -3,  4]], dtype='float64') / 30.0

    def __init__(self, workplane='XZ'):
        self._workplane = None
        self._workAxes = None
        self._setWorkplane(workplane)
        pass

    @property
    def workplane(self):
        return self._workplane

    @property
    def workAxes(self):
        return self._workAxes

    def filterOrientation(self, bar):
        selector = numpy.ix_(self.workAxes[0:2], self.workAxes[0:2])
        return bar.orientation[selector]

    def filterExyz(self, bar):
        return bar.exyz[self.workAxes[0:2]]

    def _setWorkplane(self, workplane):
        axes = {'XY': (0, 1, 2), 'XZ': (0, 2, 1), 'YZ': (1, 2, 0)}
        ax = axes.get(workplane)
        if ax is None:
            raise ValueError(f"Invalid value of workplane: {workplane}")
        self._workAxes = ax
        self._workplane = workplane


    def prepareWorkspace(self, structure, workspace):
        assert structure.geom.dim == 2, f"Invalid structure dim {structure.geom.dim}"
        ws = workspace
        geom = structure.geom
        ws.dim = geom.dim
        ws.dofsPerNode = 3
        ws.nnodes = geom.nnodes
        ws.elems = geom.elems
        ws.setupMap('dof', self.__class__.DOFNAMES)
        ws.setupMap('load', self.__class__.LOADNAMES)

        nodeLocDofs = numpy.arange(ws.dofsPerNode, dtype='uint')
        localDofs = numpy.tile(nodeLocDofs, ws.nodesPerElem)
        ws.edofs = numpy.repeat(ws.elems*ws.dofsPerNode, ws.dofsPerNode, axis=1)+localDofs
        ws.setGlobalArrays(d=1, R=1, F=1, K=2)

    def gatherInternalForces(self, structure, workspace):
        """Calculate and store internal forces in elements"""
        workspace.setElemArrays(N=1)
        for bar in structure.geom.bars:
            E = structure.materials[bar.section.material].E
            A = bar.section.area
            J = bar.section.inertia[self.workAxes[2]]
            ep = [E, A, J]
            d = workspace.get('d')
            ed = d[workspace.edofs[bar.idx]]
            N = workspace.get('N')
            N[bar.idx] = cfc.beam2e(*bar.exyz, ep, ed)

    def calcElemMatrices(self, structure, barIdx):
        return {'K': self.calcLinearStiffnessMatrix(structure, barIdx)}

    def calcLinearStiffnessMatrix(self, structure, barIdx):
        bar = structure.geom.bars[barIdx]
        E = structure.materials[bar.section.material].E
        A = bar.section.area
        J = bar.section.inertia[self.workAxes[2]]
        ep = [E, A, J]
        return cfc.beam2e(*self.filterExyz(bar), ep)

    def calcInitialStressMatrix(self, structure, barIdx, workspace):
        bar = structure.geom.bars[barIdx]
        N = workspace.N[barIdx]
        L = bar.length
        C = numpy.eye(3, dtype='float64')
        C[numpy.ix_([0, 1], [0, 1])] = self.filterOrientation(bar)
        T = numpy.kron(numpy.eye(2), C)
        Ksigma = numpy.copy(self.KsigmaBase)
        Ksigma[numpy.xi([1, 4], [1, 4])] /= L
        Ksigma[numpy.xi([2, 5], [2, 5])] *= L
        return T.T @ Ksigma @ T


class FrameFEM(object):
    """The purpose of this class is to make FEM model of a truss tructure"""

    DOFNAMES = ['Tx', 'Ty', 'Tz', 'Rx', 'Ry', 'Rz']
    LOADNAMES = ['Fx', 'Fy', 'Fx', 'Mx', 'My', 'Mz']

    def __init__(self):
        pass

    def prepare_workspace(self, structure, workspace):
        pass


def femFactory(structure):
    fems = {'Truss': {3 : Truss3D},
            'Frame': {2 : Frame2D}}
    key = type(structure).__name__
    dim = structure.geom.dim
    femfamily = fems.get(key)
    if femfamily is None:
        raise RuntimeError(f"No FEM for structure of class {key}")
    fem = femfamily.get(dim)
    if fem is None:
        raise RuntimeError("No FEM for structure of class {key} of dimension {dim}")
    return fem()


class Statics(Problem):
    def __init__(self):
        super().__init__()
        self.activeLoadCase = None

    def solve(self, structure, *args, **kwargs):
        self.report('Solving Statics')
        self._prepare_workspace(structure)
        self._assemble_supports(structure)
        self._assemble_elements(structure)
        self._assemble_loads(structure, caseName=self.activeLoadCase)
        self._solve_linear_system(structure)
        self._postprocess_elements(structure)


class InitialBuckling(Problem):
    def __init__(self):
        super().__init__()
        self.activeLoadCase = None
        self.statics = Statics()

    def solve(self, structure, *args, **kwargs):
        self.report('Solving Initial Buckling')
        self.statics.solve(structure)
        self.workspace = self.statics.workspace
        self.fem = self.statics.fem
        self._assemble_initial_stress_matrix(structure)
        self._solve_eigenproblem(structure)

    def _assemble_initial_stress_matrix(self, structure):
        self.workspace.setGlobalArrays(Ksigma=2)
        Ksigma = self.workspace.get('Ksigma')
        for elemIdx in range(self.workspace.nelems):
            Ke = self.fem.calcInitialStressMatrix(structure, elemIdx, self.workspace)
            edofs = self.workspace.edofs[elemIdx, :]
            cfc.assem(edofs, Ksigma, Ke)

    def _solve_eigenproblem(self, structure):
        self.workspace.setGlobalArrays(eigval=1, eigvec=2)
        K, Ksigma = self.workspace.get('K', 'Ksigma')
        self.workspace.eigval, self.workspace.eigvec = eigen(K, Ksigma, self.workspace.bcDofs)


def eigen(K, M, bcDofs, **kwargs):
    ndofs = K.shape[0]
    mask = numpy.ones(ndofs, 'bool')
    mask[bcDofs] = False
    freeDofs = numpy.arange(ndofs)
    freeDofs = freeDofs[mask]
    selector = numpy.ix_(freeDofs, freeDofs)
    Mr = M[selector]
    Kr = K[selector]
    eigval, eigvec_r = scipy.linalg.eig(Kr, Mr, **kwargs)
    eigvec = numpy.zeros((ndofs, freeDofs.size))
    eigvec[freeDofs, :] = eigvec_r
    return eigval, eigvec
