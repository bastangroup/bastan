bastan.fem package
==================

.. automodule:: bastan.fem
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   bastan.fem.structural

Submodules
----------

.. toctree::
   :maxdepth: 4

   bastan.fem.problems
