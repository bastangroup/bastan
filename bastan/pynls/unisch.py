"""
This file contains base class routines which are common among
number of different control algorithm solution classes.

Format equation:
  [a](i)(j-1) {dx}(i)(j) = lambda(i)(j) {r} + {u}(i)(j-1)

   where:
    [a]     -  Tangent matrix
    {dx}    -  Incremental state vector
    lambda  -  Incremental reference vector parameter
    {r}     -  Reference vector
    {u}     -  Unbalance vector
    (i)(j)  -  (Step)(Iteraction)

 Reference:
   "Solution Method for Nonlinear Problems with Multiple
    Critical Points", Yeong-Bin Yang and Ming-Shan Shieh,
   AIAA Journal, Vol 28, No 12, 1990.
"""


import abc
import numpy
import time
import os
import sys

try:
    os.environ["BASTAN_DIR"]
except KeyError:
    print("Please set the environment variable BASTAN_DIR")
    sys.exit(1)

sys.path.append(os.environ['BASTAN_DIR'])

from bastan.pynls import ctrl


class UnifiedSchemes(ctrl.Control):
    def __init__(self, model, control, linsys):
        super().__init__(model, control, linsys)
        N = self.NumEq
        self._pdDx1 = numpy.empty(N, dtype='float64')  # Incremental state vector due to reference vector
        self._pdDx2 = numpy.empty(N, dtype='float64')  # Incremental state vector due to unbalance vector
        self._dLambda = None                           # Incremental reference vector parameter
        self._pdUe = numpy.empty(N, dtype='float64')   # External contribution for unbalance vector
        self._pdUi = numpy.empty(N, dtype='float64')   # Internal contribution for unbalance vector
        self._pdX = numpy.empty(N, dtype='float64')    # Total state variable vector - total displacement
        self._pdR = numpy.empty(N, dtype='float64')    # Total state variable vector

        self._dNormReference = numpy.linalg.norm(self._pdReference)  # Euclidean norm of reference vector

    # noinspection PyPep8Naming
    @abc.abstractmethod
    def Lambda(self):
        ...

    def _CheckConvergence(self):
        pass

    def Solver(self):
        tic = time.perf_counter()

        # initialize current step
        self._iCurrStep = 1

        # Outer Loop - Continues until max number of steps is reached or until diverges
        while True:
            # initialize current iteration and set convergence to false
            self._iConvFlag = False
            self._iCurrIte = 1

            # Inner loop - Continues until convergence or max permitted iterations is exceeded (ie divergence)
            while True:
                # Compute tangent matrix if first iteration or if this is a standard update
                # (ie compute K at every iteration
                # If this is modified then only computer tangent matrix on first iteration
                if self._iCurrIte == 1 or self._sControl.updateType == ctrl.UpdateType.STANDARD:
                    # get tangent matrix
                    self._pcModel.TangentMatrix(self._pdX, self._pcLinSys)
                    # _pcLinSys solves Ax=b where A is _paA stored in _pcLinSys,
                    # and b is _pdDx1, _pdReference is copied to
                    # _pdDx1, the solver modifies b to be x, so result is _pdDx1 which is x
                    # solving K*du_I = p, so _pdDx1 is du_I
                    self._pcLinSys.Solve(self._pdReference, self._pdDx1)

                if self._iCurrIte == 1:
                    # zero out _pdDx2
                    self._pdDx2.fill(0.0)
                else:
                    # _pcLinSys solves Ax=b where A is _paA stored in _pcLinSys, and b is _pdDx2, _pdR is copied to
                    # _pdDx1, the solver modifies b to be x, so result is _pdDx2 which is x
                    # solving K*du_II = r, so _pdDx2 is du_II
                    self._pcLinSys.Solve(self._pdR, self._pdDx2)

                # compute iterative load factor
                self.Lambda()

                # update total load factor for this step (outer loop)
                self._dTotFactor += self._dLambda

                # compute _pdUe = _dLambda*_pdReference - external load vector for this iteration
                self._pdUe += self._dLambda * self._pdReference

                # compute _pdX = _dLambda*_pdDx1 + _pdDx2, ie u = d_lambda*du_I + du_II
                self._pdX += self._dLambda * self._pdDx1 + self._pdDx2

                # sets up internal vector, gets stored in _pdUi
                self._pcModel.InternalVector(self._pdX, self._pdUi)

                # calculate residual
                self._iCurrIte += 1
                if self.CheckConvergence() or self._iCurrIte >= self._sControl.numMaxIte:
                    break

            # print results of convergering or not
            if self._iConvFlag:
                self._pcModel.ReportConvergence(self._dTotFactor, self._pdX)
            else:
                print(f"\n\n\t ### Convergence not achieved Step {self._iCurrStep} !!! ###\n")
                break

            self._iCurrStep += 1
            if self._iCurrStep > self._sControl.numMaxStep:
                break

        toc = time.perf_counter()
        print(f"Solution time= {toc - tic:0.4f} seconds")

    def CheckConvergence(self):
        # assign external load vector to residual, external load vector is all of the previous
        # external loads plus the lambda*referenceLoad for this iteration
        numpy.copyto(self._pdR, self._pdUe)

        # right hand side of "governing" equation, ie K_(j-1)*deltaU_j = p_j - f_(j-1)
        # external load at j - internal load at j-1
        # _pdUe = _pdUe-_pdUi = p_j-1+lambda*p_bar-f_j-1
        self._pdR -= self._pdUi

        error_val = numpy.linalg.norm(self._pdR)

        error_val /= self._dNormReference

        self._iConvFlag = error_val <= self._sControl.tol

        return self._iConvFlag
