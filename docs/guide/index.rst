.. BASTAN documentation master file, created by
   sphinx-quickstart on Fri Aug  9 17:36:16 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BASANS's documentation!
===================================

.. toctree::
   :maxdepth: 5
   :caption: Contents:

   source/intro/index
   source/user_guide/index
   auto_examples/index
   source/tools/index
   source/developer_guide/index
   source/reference/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
