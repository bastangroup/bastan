import os
import sys
import numpy
import copy
from matplotlib import pyplot as plt

try:
    os.environ["BASTAN_DIR"]
except KeyError:
    print("Please set the environment variable BASTAN_DIR")
    sys.exit(1)

sys.path.append(os.environ['BASTAN_DIR'])

from bastan import pynls


class ModelFunction1D(pynls.model.Model):
    def __init__(self, report_file=''):
        super().__init__()
        self._iNumEq = 1
        self._iNumGra = 1
        self.plt_x = []
        self.plt_y = []
        self.report_file = report_file

    def Init(self):
        self.plt_x = [0]
        self.plt_y = [0]
        if self.report_file:
            self._outfile = open(self.report_file, 'w')

    def InternalVector(self, u, f):
        x = u[0]-1.0
        f[0] = -3*numpy.sign(x) * numpy.fabs(x)**(1.0/3.0) + 4 * x + 1

    def Reference(self, f):
        f[0] = 1

    def TangentMatrix(self, u, lin_sys):
        x = u[0] - 1.0
        if numpy.fabs(x) < 1e-10:
            k = -1e32
        else:
            k = -1 / numpy.fabs(x)**(2/3.0) + 4
        lin_sys.AddA(0, 0, k)

    def ReportConvergence(self, dFactor, pdSol):
        if self._outfile:
            self._outfile.write(f"{pdSol[0]}  {dFactor}\n")
        self.plt_x.append(pdSol[0])
        self.plt_y.append(dFactor)


if __name__ == '__main__':
    model = ModelFunction1D()
    linSys = pynls.linearsystem.LinSys()
    ctr = pynls.ctrl.ControlParams()

    ctr.updateType = pynls.ctrl.UpdateType.STANDARD
    ctr.ctrlFactor = 0.02
    ctr.ctrlType = pynls.ctrl.ControlType.CONSTANT

    ctr.numMaxStep = 475
    ctr.numMaxIte = 60
    ctr.tol = 0.1 

    model.Init()

    control_arc = pynls.ArcLengthControl(model, ctr, linSys)
    control_arc.Solver()
    arc_x = copy.copy(model.plt_x)
    arc_y = copy.copy(model.plt_y)


    modelA = ModelFunction1D()
    linSysA = pynls.linearsystem.LinSys()
    ctrA = pynls.ctrl.ControlParams()

    ctrA.updateType = pynls.ctrl.UpdateType.STANDARD
    ctrA.ctrlType = pynls.ctrl.ControlType.CONSTANT
    ctrA.numMaxIte = 20
    ctrA.ctrlFactor = 0.11
    ctrA.numMaxStep = 20

    modelA.Init()
    control_disp = pynls.DisplacementControl(modelA, ctrA, linSysA)
    control_disp.Solver()
    disp_x = modelA.plt_x
    disp_y = modelA.plt_y

    modelB = ModelFunction1D()
    linSysB = pynls.linearsystem.LinSys()
    ctrB = pynls.ctrl.ControlParams()

    ctrB.updateType = pynls.ctrl.UpdateType.MODIFIED
    ctrB.ctrlType = pynls.ctrl.ControlType.CONSTANT
    ctrB.numMaxIte = 10
    ctrB.ctrlFactor = 0.1
    ctrB.numMaxStep = 19
    ctrB.tol = 0.1

    modelB.Init()
    control_nr = pynls.NewtonRaphsonControl(modelB, ctrB, linSysB)
    control_nr.Solver()
    nr_x = modelB.plt_x
    nr_y = modelB.plt_y


    plt.plot(arc_x, arc_y, '-')
    plt.plot(disp_x, disp_y, "-+")
    plt.plot(nr_x, nr_y, "-x")
    plt.show()

