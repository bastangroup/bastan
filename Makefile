BASTAN_CONFIG_DIR=$(shell pwd)/config

all: 
	@echo Config dir: $(BASTAN_CONFIG_DIR)
	@echo "Targets are: testall(aka: alltest) allexamples"

include config/Makefile.mk

alltest: testall

alltests: testall

testall:
	@echo "Run all tests"
	cd bastan
	python -m unittest discover

allexamples:
	cd bastan/examples && make allexamples
