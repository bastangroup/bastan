import os
import sys

try:
    os.environ["BASTAN_DIR"]
except KeyError:
    print("Please set the environment variable BASTAN_DIR")
    sys.exit(1)

sys.path.append(os.environ['BASTAN_DIR'])

