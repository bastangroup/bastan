# -*- coding: utf-8 -*-
"""
Dummy Hello World Example
=========================

"""

__authors__ = 'RP, JP'
__copyright__ = '(c) 2020 BASTAN Group '
__date__ = 'Fri, Apr 17, 2020 12:07:56 AM'
__version__ = '0.1.0'

import numpy as np
import os
import sys

try:
    os.environ["BASTAN_DIR"]
except KeyError:
    print("Please set the environment variable BASTAN_DIR")
    sys.exit(1)

sys.path.append(os.environ['BASTAN_DIR'])

import bastan

PKGDIR = os.path.dirname(bastan.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')

class DemoHelloWorld(bastan.examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        print('Hello world')
        print(f"PKGDIR is : {PKGDIR}")
        print(f"DATADIR is : {DATADIR}")

if __name__ == '__main__':
    demo = DemoHelloWorld()
    demo.run()
