# -*- coding: utf-8 -*-
"""
Plot iterative process for von Misses truss
===========================================

"""

__authors__ = 'RP'
__copyright__ = '(c) 2021 BASTAN Group'
__date__ =  "Tue Apr  6 09:58:57 CEDT 2021"
__version__ = '0.1.0'

import numpy as np
import os
import sys

try:
    os.environ["BASTAN_DIR"]
except KeyError:
    print("Please set the environment variable BASTAN_DIR")
    sys.exit(1)

sys.path.append(os.environ['BASTAN_DIR'])

import bastan
import json

PKGDIR = os.path.dirname(bastan.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')

class DemoMissesShowIterativeProcess(bastan.examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        plotter = bastan.structures.MissesPlotter()
        vmt = bastan.structures.MissesTruss(L=1.0, H=0.2, E=1, A=1, P=1)
        lpt = vmt.get_limit_point(0)
        plotter.plot_analitic_equilibrium_path(vmt, etamax=0.1)
        q=0
        deltap = 0.1
        plotter.plot_iteration_step_load_control(vmt, q, deltap)
        plotter.show()

if __name__ == '__main__':
    demo = DemoMissesShowIterativeProcess()
    demo.run()
