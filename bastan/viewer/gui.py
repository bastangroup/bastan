from ui_camera import Ui_CameraGUI

import math
import numpy

from PySide2.QtCore import Slot
from PySide2.QtWidgets import QFrame
from PySide2.QtGui import QVector3D

from PySide2.Qt3DRender import Qt3DRender
from PySide2.Qt3DCore import Qt3DCore

from bastan import bbox3D

def sliderFraction(slider):
    return slider.value()/float(slider.maximum())


def setSliderValue(slider, fraction):
    value = int(round((1-fraction)*slider.minimum() + fraction*slider.maximum()))
    return slider.setValue(value)


class CameraGUI(QFrame):
    def __init__(self, parent):
        super().__init__(parent)
        self.ui = Ui_CameraGUI() 
        self.ui.setupUi(self)
        self.ui.bboxAlignFactor.setValue(1.0)
        self.camera = None
        self.bbox = bbox3D.AABB((-1,-1,-1),(1,1,1))

        self.lightEntity = Qt3DCore.QEntity()

        self.cameraLight = Qt3DRender.QPointLight(self.lightEntity)
        self.cameraLight.setColor("white")
        self.cameraLight.setIntensity(1)

        self.lightEntity.addComponent(self.cameraLight)

    def setBBox(self, bbox):
        self.bbox = bbox
        if self.ui.followBBoxAlign.isChecked():
            self.updateCameraOnBBoxAlign()

    def setCamera(self, camera):
        self.camera = camera

        self.camera.lens().setPerspectiveProjection(45.0, 16.0/9.0, 0.1, 1000.0)
        self.camera.setPosition(QVector3D(0, 0, 20.0))
        self.camera.setViewCenter(QVector3D(0, 0, 0))
        self.camera.setUpVector(QVector3D(0, 0, 1))

        self.lightEntity.setParent(self.camera)

    def resetBBoxAlign(self):
        setSliderValue(self.ui.xFractionBB, 1.0)
        setSliderValue(self.ui.yFractionBB, 1.0)
        setSliderValue(self.ui.zFractionBB, 1.0)
        self.ui.bboxAlignFactor.setValue(1.0)

    def resetView(self):
        self.resetBBoxAlign()
        self.updateCameraOnBBoxAlign()
        self.camera.setUpVector(QVector3D(0, 0, 1))

    def setCameraPosition(self, pos, updatePosWidget=True):
        if updatePosWidget:
            self.updateCameraPositionWidget(pos)
        self.camera.setPosition(QVector3D(pos[0], pos[1], pos[2]))
        self.camera.setUpVector(QVector3D(0, 0, 1))

    def updateCameraOnBBoxAlign(self):
        xfraction = sliderFraction(self.ui.xFractionBB)
        yfraction = sliderFraction(self.ui.yFractionBB)
        zfraction = sliderFraction(self.ui.zFractionBB)
        scale = self.ui.bboxAlignFactor.value()
        pt = self.bbox.getFractionPoint(xfraction, yfraction, zfraction, scale=scale)
        update = self.ui.followBBoxAlign.isChecked()
        self.setCameraPosition(pt, update)

    def updateCameraPositionWidget(self, pos):
        self.ui.cameraPositionX.setValue(pos[0])
        self.ui.cameraPositionY.setValue(pos[1])
        self.ui.cameraPositionZ.setValue(pos[2])

    def setCameraViewCenter(self, pos):
        self.camera.setViewCenter(QVector3D(pos[0], pos[1], pos[2]))
        self.updateLookAtWidget(pos)

    def updateLookAtWidget(self, pos):
        self.ui.cameraLookAtX.setValue(pos[0])
        self.ui.cameraLookAtY.setValue(pos[1])
        self.ui.cameraLookAtZ.setValue(pos[2])

    def orbitCameraXY(self, angle):
        pos = self.camera.position()-self.camera.viewCenter()
        r = numpy.sqrt(pos.x()**2+pos.y()**2)
        pos.setX(r*numpy.cos(angle)+self.camera.viewCenter().x())
        pos.setY(r*numpy.sin(angle)+self.camera.viewCenter().y())
        pos.setZ(self.camera.position().z())
        return (pos.x(), pos.y(), pos.z())

    def orbitCameraZX(self, angle):
        pos = self.camera.position()-self.camera.viewCenter()
        r = numpy.sqrt(pos.z()**2+pos.x()**2)
        pos.setZ(r*numpy.cos(angle)+self.camera.viewCenter().z())
        pos.setX(r*numpy.sin(angle)+self.camera.viewCenter().x())
        pos.setY(self.camera.position().y())
        return (pos.x(), pos.y(), pos.z())

    def orbitCameraYZ(self, angle):
        pos = self.camera.position()-self.camera.viewCenter()
        r = numpy.sqrt(pos.y()**2+pos.z()**2)
        pos.setY(r*numpy.cos(angle)+self.camera.viewCenter().y())
        pos.setZ(r*numpy.sin(angle)+self.camera.viewCenter().z())
        pos.setX(self.camera.position().x())
        return (pos.x(), pos.y(), pos.z())

    @Slot()
    def onCameraXYZChange(self, _):
        self.setCameraPosition((self.ui.cameraPositionX.value(),
                               self.ui.cameraPositionY.value(),
                               self.ui.cameraPositionZ.value()))
        x = self.ui.cameraLookAtX.value()
        y = self.ui.cameraLookAtY.value()
        z = self.ui.cameraLookAtZ.value()
        self.setCameraViewCenter((x, y, z))

    @Slot()
    def onBBoxAlignFactorChange(self, _):
        self.updateCameraOnBBoxAlign()

    @Slot()
    def onBBoxAlignFractionChange(self, _):
        self.updateCameraOnBBoxAlign()

    @Slot()
    def onBBoxAlign(self, val):
        if val:
           self.updateCameraOnBBoxAlign()

    @Slot()
    def onLookAtOrigin(self, val):
        if val:
            self.ui.cameraLookAtX.setDisabled(True)
            self.ui.cameraLookAtY.setDisabled(True)
            self.ui.cameraLookAtZ.setDisabled(True)
            self.camera.setViewCenter(QVector3D(0, 0, 0))

    @Slot()
    def onLookAtBBoxCenter(self, val):
        if val:
            self.ui.cameraLookAtX.setDisabled(True)
            self.ui.cameraLookAtY.setDisabled(True)
            self.ui.cameraLookAtZ.setDisabled(True)
            p = self.bbox.center
            self.camera.setViewCenter(QVector3D(p[0],p[1],p[2]))

    @Slot()
    def onLookAtUserPoint(self, val):
        if val:
            self.ui.cameraLookAtX.setEnabled(True)
            self.ui.cameraLookAtY.setEnabled(True)
            self.ui.cameraLookAtZ.setEnabled(True)
            x = self.ui.cameraLookAtX.value()
            y = self.ui.cameraLookAtY.value()
            z = self.ui.cameraLookAtZ.value()
            self.setCameraViewCenter((x, y, z))
        else:
            self.ui.cameraLookAtX.setDisabled(True)
            self.ui.cameraLookAtY.setDisabled(True)
            self.ui.cameraLookAtZ.setDisabled(True)

    @Slot()
    def onCameraOrbitAngle(self, val):
        angle = 2*math.pi*sliderFraction(self.ui.orbitingSlider)
        if self.ui.xyOrbitRadio.isChecked():
            pos = self.orbitCameraXY(angle)
        elif self.ui.yzOrbitRadio.isChecked():
            pos = self.orbitCameraYZ(angle)
        elif self.ui.zxOrbitRadio.isChecked():
            pos = self.orbitCameraZX(angle)
        else:
            raise RuntimeError("On orbit plane selected")
        self.setCameraPosition(pos)
