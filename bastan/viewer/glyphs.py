import math
import os
import re
import sys
import pandas
import argparse
import json
import copy
import time
import logging

import matplotlib
import matplotlib.cm

import PySide2
from PySide2.QtWidgets import QApplication, QMainWindow, QFileDialog, QStatusBar
from PySide2.QtCore import QFile, QTimeLine, QEventLoop
from PySide2.Qt3DExtras import Qt3DExtras
from PySide2.Qt3DCore import Qt3DCore
from PySide2.QtGui import QVector3D, QColor, QVector4D, QQuaternion, QMatrix3x3
from PySide2.Qt3DRender import Qt3DRender
from PySide2.QtMultimedia import QMediaRecorder, QVideoEncoderSettings
from PySide2.QtCore import QObject, Signal, Slot

import numpy

from bastan.viewer import draw3D

# Uncomment below for terminal log messages
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())
logger.info("All import OK")

class Glyph(object):
    cmap = matplotlib.cm.get_cmap('tab20')
    cnorm = matplotlib.colors.Normalize(0,19)

    @staticmethod
    def getColor(idx):
        rgba = Glyph.cmap(Glyph.cnorm(idx))
        rgba = [int(255*i) for i in rgba]
        return QColor(*rgba)


class PlaneGlyph(Glyph):
    cmap = matplotlib.cm.get_cmap('tab20')
    cnorm = matplotlib.colors.Normalize(0,19)

    def __init__(self, rootEntity, idx=0, plane=None):
        super().__init__()
        self.idx = idx
        self.plane = plane 
        self.center = None
        self._build(rootEntity, plane)

    def _build(self, rootEntity, plane):
        self.planeMesh = Qt3DExtras.QPlaneMesh()
        self.center = plane.anchor
        self.planeMesh.setWidth(2)
        self.planeMesh.setHeight(2)
        axis = QVector3D(0,-1,0)
        normal = QVector3D(*plane.normal.tolist())
        rotation = QQuaternion.rotationTo(normal, axis)
        self.planeTransform = Qt3DCore.QTransform()
        self.planeTransform.setRotation(rotation)
        self.planeTransform.setTranslation(QVector3D(*self.center.tolist()))
        self.planeMaterial = Qt3DExtras.QPhongMaterial()
        self.planeMaterial.setDiffuse(QColor('Gray'))
        self.planeEntity = Qt3DCore.QEntity(rootEntity)
        self.planeEntity.addComponent(self.planeMesh)
        self.planeEntity.addComponent(self.planeMaterial)
        self.planeEntity.addComponent(self.planeTransform)

    def update(self, stepId, dt):
        pass


class BoxGlyph(object):
    cmap = matplotlib.cm.get_cmap('tab20')
    cnorm = matplotlib.colors.Normalize(0,19)

    def __init__(self, rootEntity, idx=0, box=None):
        logger.info("Construct box glyph")
        super().__init__()
        self.idx = idx
        self.box = box
        self.center = None
        self._build(rootEntity, box)

    def _build(self, rootEntity, box):
        self.bboxMesh = meshes3D.box2QMesh(box, rootEntity, None)
        lbf = numpy.copy(box.anchor)
        rtb = lbf + box.edges[0]+box.edges[1]+box.edges[2]
        extents = rtb-lbf
        self.center = numpy.array([0.0,0.0,0.0], dtype='float64')
        self.bboxEntity = Qt3DCore.QEntity(rootEntity)
        self.bboxTransform = Qt3DCore.QTransform()
        self.bboxTransform.setTranslation(QVector3D(*self.center.tolist()))
        self.bboxMaterial = Qt3DExtras.QPhongMaterial(rootEntity)
        self.bboxMaterial.setDiffuse(QColor('Yellow'))

        self.bboxEntity.addComponent(self.bboxMesh)
        self.bboxEntity.addComponent(self.bboxMaterial)
        self.bboxEntity.addComponent(self.bboxTransform)
        logger.debug("Finished building box glyph")

    def update(self, stepId, dt):
        logger.info(f"Updating box: f{self.idx} to step {stepId}")
        vel = self.box.linearVelocity
        accel = self.box.accelerationMagnitude * vel / numpy.linalg.norm(vel)
        t = stepId*dt
        center = self.center + vel*t + 0.5*accel*t**2
        logger.info(f"Update glyph to position {center}")
        self.bboxTransform.setTranslation(QVector3D(*center.tolist()))

class GBoxGlyph(object):
    cmap = matplotlib.cm.get_cmap('tab20')
    cnorm = matplotlib.colors.Normalize(0,19)

    def __init__(self, rootEntity, idx=0, box=None):
        logger.info("Construct box glyph")
        super().__init__()
        self.idx = idx
        self.box = box
        self.center = None
        self._build(rootEntity, box)

    def _build(self, rootEntity, box):
        self.bboxMesh = Qt3DExtras.QCuboidMesh()
        lbf = numpy.copy(box.anchor)
        rtb = lbf + box.edges[0]+box.edges[1]+box.edges[2]
        extents = rtb-lbf
        self.center = 0.5*(lbf+rtb)
        self.bboxMesh.setXExtent(extents[0])
        self.bboxMesh.setYExtent(extents[1])
        self.bboxMesh.setZExtent(extents[2])

        self.bboxEntity = Qt3DCore.QEntity(rootEntity)
        self.bboxTransform = Qt3DCore.QTransform()
        self.bboxTransform.setTranslation(QVector3D(*self.center.tolist()))
        #self.bboxMaterial = Qt3DExtras.QPhongAlphaMaterial()
        self.bboxMaterial = Qt3DExtras.QPhongMaterial(rootEntity)
        self.bboxMaterial.setDiffuse(QColor('Yellow'))
        #self.bboxMaterial.setAlpha(1.0)
        #self.bboxMesh = meshes3D.box2QMesh(box, self.bboxEntity, self.bboxMaterial)

        self.bboxEntity.addComponent(self.bboxMesh)
        self.bboxEntity.addComponent(self.bboxMaterial)
        self.bboxEntity.addComponent(self.bboxTransform)
        logger.debug("Finished building box glyph")

    def update(self, stepId, dt):
        logger.info(f"Updating box: f{self.idx} to step {stepId}")
        vel = self.box.linearVelocity
        accel = self.box.accelerationMagnitude * vel / numpy.linalg.norm(vel)
        t = stepId*dt
        center = self.center + vel*t + 0.5*accel*t**2
        logger.info(f"Update glyph to position {center}")
        self.bboxTransform.setTranslation(QVector3D(*center.tolist()))

class ABoxGlyph(Glyph):
    cmap = matplotlib.cm.get_cmap('tab20')
    cnorm = matplotlib.colors.Normalize(0,19)

    def __init__(self, rootEntity, idx=0, box=None):
        super().__init__()
        self.idx = idx
        self.box = box
        self.center = None
        self._build(rootEntity, box)

    def _build(self, rootEntity, box):

        lbf = numpy.copy(box.anchor)
        rtb = lbf + box.edges[0]+box.edges[1]+box.edges[2]
        extents = rtb-lbf
        self.center = 0.5*(lbf+rtb)

        self.anchorTransform = Qt3DCore.QTransform()
        self.anchorTransform.setTranslation(QVector3D(*self.center.tolist()))
        self.anchorEntity = Qt3DCore.QEntity(rootEntity)
        self.anchorEntity.addComponent(self.anchorTransform)

        self.bboxMesh = Qt3DExtras.QCuboidMesh()
        self.bboxMesh.setXExtent(extents[0])
        self.bboxMesh.setYExtent(extents[1])
        self.bboxMesh.setZExtent(extents[2])
        self.bboxTransform = Qt3DCore.QTransform()
        m = self.bboxTransform.matrix()
        for i in range(3):
            e = box.edges[i]
            e = e/numpy.linalg.norm(e)
            print(e)
            v = QVector4D(e[0], e[1], e[2], 0)
            m.setColumn(i, v)
        print(m)
        self.bboxTransform.setMatrix(m)

        self.bboxMaterial = Qt3DExtras.QPhongAlphaMaterial()
        self.bboxMaterial = Qt3DExtras.QPhongMaterial()
        self.bboxMaterial.setDiffuse(QColor('Yellow'))
        #self.bboxMaterial.setAlpha(1.0)
        self.bboxEntity = Qt3DCore.QEntity(self.anchorEntity)
        self.bboxEntity.addComponent(self.bboxTransform)
        self.bboxEntity.addComponent(self.bboxMesh)
        self.bboxEntity.addComponent(self.bboxMaterial)

    def update(self, stepId, dt):
        vel = self.box.linearVelocity
        accel = self.box.accelerationMagnitude * vel / numpy.linalg.norm(vel)
        t = stepId*dt
        center = self.center + vel*t + 0.5*accel*t**2
        self.anchorTransform.setTranslation(QVector3D(*center.tolist()))


class BarGlyph(Glyph):
    count = 0
    alpha = 0.8
    radius = 0.02
    def __init__(self, rootEntity, bar):
        super().__init__()
        self.coordSys = None
        self.bar = bar
        self._build(bar, rootEntity)

    def _build(self, bar, rootEntity):
        if bar is None:
            print("No data for bar glyph")
        self.barMesh = Qt3DExtras.QCylinderMesh()
        self.barMesh.setRings(5)
        self.barMesh.setSlices(12)
        self.barMesh.setRadius(BarGlyph.radius)
        self.barMesh.setLength(bar.length)

        self.anchorTransform = Qt3DCore.QTransform()
        self.anchorTransform.setTranslation(QVector3D(*bar.center))
 
        aX = QVector3D(*bar.axisX)
        aY = QVector3D(*bar.axisY)
        aZ = QVector3D(*bar.axisZ)
        orient = QQuaternion.fromAxes(aX, aY, aZ)
        self.anchorTransform.setRotation(orient)

        self.anchorEntity = Qt3DCore.QEntity(rootEntity)
        self.anchorEntity.addComponent(self.anchorTransform)

        self.barEntity = Qt3DCore.QEntity(self.anchorEntity)
        orient = QQuaternion.rotationTo(QVector3D(0,1,0), QVector3D(1,0,0))
        self.barTransform = Qt3DCore.QTransform()
        self.barTransform.setRotation(orient)
        self.barEntity.addComponent(self.barTransform)

        self.barMaterial = Qt3DExtras.QPhongAlphaMaterial()
        self.barMaterial.setAlpha(BarGlyph.alpha)
        self.barMaterial.setDiffuse(BarGlyph.getColor(bar.idx))

        self.barEntity.addComponent(self.barTransform)
        self.barEntity.addComponent(self.barMaterial)
        self.barEntity.addComponent(self.barMesh)
        self.coordSys = draw3D.CoordSys(parent=self.anchorEntity, lengths=(0.5, 0.5, 0.5))

    def update(self, stepId):
        pass

class EllipsoidGlyph(Glyph):
    def __init__(self, rootEntity, idx=0, ellipsoid=None):
        super().__init__()
        self.idx = idx
        self.coordSys = None
        self.boneAxis = None
        self.body = ellipsoid
        self._mesh = None
        self._build(rootEntity, ellipsoid)
        self.orientationData = numpy.empty((0,9), dtype='float64')

    def setBodyVisible(self, flag):
        self._mesh.setEnabled(flag)

    def setOrientation(self, orientationData):
        self.orientationData = orientationData

    def setLinearPosition(self, positionData):
        self.linearPosition = positionData

    def _build(self, rootEntity, ellipsoid):
        if ellipsoid is None:
            print("No data for ellipsoid glyph")
        self._mesh = Qt3DExtras.QSphereMesh()
        self._mesh.setRings(20)
        self._mesh.setSlices(20)
        self._mesh.setRadius(1)

        print('Elipsoid at: ', ellipsoid.linearPosition)
        self.sphereTransform = Qt3DCore.QTransform()
        self.sphereTransform.setScale3D(QVector3D(*ellipsoid.semiAxesLength))
        self.sphereTransform.setTranslation(QVector3D(*ellipsoid.linearPosition.tolist()))
        self.sphereTransform.setRotation(ellipsoid.rotation())

        self.sphereMaterial = Qt3DExtras.QPhongMaterial()
        self.sphereMaterial.setDiffuse(EllipsoidGlyph.getColor(self.idx))
        #self.sphereMaterial.setProperty('alpha', 0.5)
        #print(self.sphereMaterial.property('alpha'))
        #print(self.sphereMaterial.__dir__())
        #self.sphereMaterial.dumpObjectInfo()
        #self.sphereMaterial.dumpObjectTree()
        #print(self.sphereMaterial.parameters())

        self.sphereEntity = Qt3DCore.QEntity(rootEntity)
        self.sphereEntity.addComponent(self._mesh)
        self.sphereEntity.addComponent(self.sphereMaterial)
        self.sphereEntity.addComponent(self.sphereTransform)
        lengths = 1.2*numpy.array([1.0,1.0,1.0])
        self.coordSys = draw3D.CoordSys(parent=self.sphereEntity, lengths=lengths, symmetric=True)

    def update(self, stepId):
        m = self.orientationData[stepId, :]
        M = QMatrix3x3(m)
        q = QQuaternion.fromRotationMatrix(M)
        self.sphereTransform.setRotation(q)
        r = self.linearPosition[stepId, :]
        self.sphereTransform.setTranslation(QVector3D(r[0], r[1], r[2]))


class JointGlyph(object):
    counter = 0
    radius = 0.1
    color = "Black"

    def __init__(self, parent, anchor=QVector3D(0, 0, 0), rotation=QQuaternion()):
        self.id = JointGlyph.counter
        JointGlyph.counter += 1
        self.sphereMesh = Qt3DExtras.QSphereMesh()
        self.sphereMesh.setRings(10)
        self.sphereMesh.setSlices(10)
        self.sphereMesh.setRadius(JointGlyph.radius)
        self.sphereMaterial = Qt3DExtras.QPhongMaterial()
        self.sphereMaterial.setDiffuse(JointGlyph.color)

        self.jointTransform = Qt3DCore.QTransform()
        self.jointTransform.setRotation(rotation)
        self.jointTransform.setTranslation(anchor)

        logger.debug(f"CREATE Joint: {self.id} at : {ppqt(anchor)}")

        self.jointEntity = Qt3DCore.QEntity(parent)
        self.jointEntity.addComponent(self.jointTransform)
        self.jointEntity.addComponent(self.sphereMesh)
        self.jointEntity.addComponent(self.sphereMaterial)

    def rotation(self):
        return self.jointTransform.rotation()

    def setX(self, angle): 
        self.jointTransform.setRotationX(angle)

    def setY(self, angle):
        self.jointTransform.setRotationY(angle)

    def setZ(self, angle):
        self.jointTransform.setRotationZ(angle)

    def getX(self):
        return self.jointTransform.rotationX()

    def getY(self):
        return self.jointTransform.rotationY()

    def getZ(self):
        return self.jointTransform.rotationZ()


if __name__ == "__main__":
    pass
