# -*- coding: utf-8 -*-
"""
Statics of von Misses truss
===========================

"""

__authors__ = 'RP, JP'
__copyright__ = '(c) 2020 BASTAN Group'
__date__ = 'Fri, Apr 17, 2020 12:07:56 AM'
__version__ = '0.1.0'

import numpy as np
import os
import sys

try:
    os.environ["BASTAN_DIR"]
except KeyError:
    print("Please set the environment variable BASTAN_DIR")
    sys.exit(1)

sys.path.append(os.environ['BASTAN_DIR'])

import bastan
import json

PKGDIR = os.path.dirname(bastan.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


VON_MISSES_TRUSS_DATA = """
{
  "params" : {
    "a" : 1.0,
    "P" : 10.0
  },
  "Truss" : {
    "bargeom" : {
      "dim" : 3,
      "coords" : [
        "-a", 0.0, 0.0,
        0.0, 0.0, "0.5*a",
        "a", 0.0, 0.0],
      "elems" : [0,1,
                 1,2],
      "sections" : {
        "sec_all" : {
          "forBars" : ["0-1"],
          "area" : 1.0,
          "material" : "mat_all"
        }
      }
    },
    "materials" : {
      "mat_all" : {
        "E" : 1.0
      }
    },
    "supports" : {
      "pin" : {
        "Tx" : "Fixed",
        "Tz" : "Fixed",
        "atNodes" : [0,2]
      },
      "inplane" : {
        "Ty" : "Fixed",
        "atNodes" : ["0-2"]
      }
    },
    "loads" : {
      "cases" : {
        "live_1" : {
          "nodal" : {
            "ForceZ" : {
              "Fz" : "-2*P",
              "atNodes" : [1]
            }
          }
        }
      }
    }
  }
}
"""

class DemoMissesStatics(bastan.examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        data = json.loads(VON_MISSES_TRUSS_DATA)
        bastan.jsonsupport.setupParams(data)
        structure = bastan.structures.Truss.fromData(data)
        problem = bastan.fem.Statics()
        problem.solve(structure)

if __name__ == '__main__':
    demo = DemoMissesStatics()
    demo.run()
