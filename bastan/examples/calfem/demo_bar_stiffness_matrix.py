# -*- coding: utf-8 -*-
"""
Stiffness matrix of a bar element
=================================

"""

__authors__ = 'RP, JP'
__copyright__ = '(c) 2020 BASTAN Group'
__date__ = 'Fri, Apr 17, 2020 12:07:56 AM'
__version__ = '0.1.0'

import numpy as np
import os
import sys

try:
    os.environ["BASTAN_DIR"]
except KeyError:
    print("Please set the environment variable BASTAN_DIR")
    sys.exit(1)

sys.path.append(os.environ['BASTAN_DIR'])

import bastan
#import calfem.core as cfem 

PKGDIR = os.path.dirname(bastan.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')

class DemoAla(bastan.examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
       EA = 1
       #K = cfem.bar1e(EA)
       print(f"Bar stiffness matrix for EA = {EA}")
       #print(K)

if __name__ == '__main__':
    demo = DemoAla()
    demo.run()
