# -*- coding: utf-8 -*-
"""
Defining and visualising von Misses truss
=========================================

"""

__authors__ = 'RP'
__copyright__ = '(c) 2021 BASTAN Group'
__date__ =  "Tue Mar 30 12:21:14 CEDT 2021"
__version__ = '0.1.0'

import numpy as np
import os
import sys

try:
    os.environ["BASTAN_DIR"]
except KeyError:
    print("Please set the environment variable BASTAN_DIR")
    sys.exit(1)

sys.path.append(os.environ['BASTAN_DIR'])

import bastan
import json

PKGDIR = os.path.dirname(bastan.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')

class DemoMissesTruss(bastan.examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        vmt = bastan.structures.MissesTruss(L=1.0, H=0.2, E=1, A=1, P=1)
        print(vmt)
        control = [0.1, 0.1, 0.1, 0.08]
        q, lq, c = vmt.solve_equilibrium(control, epsR=1e-4, epsq=1e-4)
        for i in range(len(control)):
            print(q[i], lq[i], c[i])

if __name__ == '__main__':
    demo = DemoMissesTruss()
    demo.run()
