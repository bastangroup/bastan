# -*- coding: utf-8 -*-
"""
Example how to read JSON file with structure definition
=======================================================

"""

__authors__ = 'RP'
__copyright__ = '(c) 2020 BASTAN Group'
__date__ = 'Fri, Apr 17, 2020 12:07:56 AM'
__version__ = '0.1.0'

import numpy as np
import os
import sys

try:
    os.environ["BASTAN_DIR"]
except KeyError:
    print("Please set the environment variable BASTAN_DIR")
    sys.exit(1)

sys.path.append(os.environ['BASTAN_DIR'])

import bastan
import json

PKGDIR = os.path.dirname(bastan.__file__)
DATADIR = os.path.join(PKGDIR, '..', 'data')


class DemoReadingJSON(bastan.examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        with open(os.path.join(DATADIR, "truss_doom.json"), "r") as read_file:
            data = json.load(read_file)
            bastan.jsonsupport.setupParams(data)
            mytruss = bastan.structures.Truss.fromData(data)
            for i, b in enumerate(mytruss.geom.bars):
                print(f"Bar {i} start point {b.startPoint}   end point {b.endPoint}")

        othertruss = bastan.structures.Truss.fromJSON(os.path.join(DATADIR, "truss_doom.json"))
        print("Number of bars : ", len(othertruss.geom.bars))

if __name__ == '__main__':
    demo = DemoReadingJSON()
    demo.run()
