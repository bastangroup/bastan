# -*- coding: utf-8 -*-
"""
Module providing data structures bar cross-sections 
"""

import numpy
import math

from bastan import jsonsupport
from bastan.jsonsupport import JSONBased, JSONBasedCollection

class Section(JSONBased):
    JSONattributes = [
            ("area", jsonsupport.nochange),
            ("material", jsonsupport.nochange),
            ("forBars", jsonsupport.asranges)
                ]
    counter = 0
    @staticmethod
    def getName(name):
        if name is None:
            Section.counter += 1
            name = f"section_{counter}"
        return name

    def __init__(self, name=None):
        self.area = 0.0
        self.material = None
        self.forBars = []
        self.name = Section.getName(name)


class Sections(JSONBasedCollection):
    JSONkey = 'sections'
    JSONattributes = []
    ItemClass = Section 

    def __init__(self):
        super().__init__()
