# -*- coding: utf-8 -*-
"""
Module providing data structures describing materials 
"""

from bastan import jsonsupport
from bastan.jsonsupport import JSONBased, JSONBasedCollection

class Material(JSONBased):
    JSONattributes = [
            ("E", jsonsupport.nochange)
                ]
    counter = 0
    @staticmethod
    def getName(name):
        if name is None:
            Material.counter += 1
            name = f"material_{counter}"
        return name

    def __init__(self, name=None):
        self.E = 0.0
        self.name = Material.getName(name)


class Materials(JSONBasedCollection):
    JSONkey = 'materials'
    JSONattributes = []
    ItemClass = Material

    def __init__(self):
        super().__init__()
