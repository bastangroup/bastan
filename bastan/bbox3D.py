# -*- coding: utf-8 -*-
"""Module for manipulation of bounding boxes.
"""

__authors__ = 'RP'
__copyright__ = 'Roman Putanowicz'
__date__ = '9/10/2019'
__version__ = '0.1.0'

import copy
import math
import numpy

class AABB(object):
    """Axis aligned bounding box"""
    points = numpy.array([[0, 0, 0], [1, 0, 0], [1, 1, 0], [0, 1, 0],
                         [0, 0, 1], [1, 0, 1], [1, 1, 1], [0, 1, 1]], dtype='bool')
    edges = {'bottom-front': (0, 1), 'bottom-back': (3, 2),
             'top-front': (4, 5),    'top-back': (7, 6),
             'bottom-left': (0, 3),  'bottom-right': (1, 2),
             'top-left': (4, 7),     'top-right': (5, 6),
             'left-front': (0, 4),   'left-back': (3, 7),
             'right-front': (1, 5),  'right-back': (2, 6)
             }

    def __init__(self, lbf_corner=(0.0, 0.0, 0.0), rtb_corner=(0.0, 0.0, 0.0)):
        self._corners = numpy.array([lbf_corner, rtb_corner], dtype=numpy.float64)

    @classmethod
    def fromCenterAndSize(cls, center=(0.0, 0.0, 0.0), size=1.0):
        center = numpy.asarray(center)
        lbf_corner = numpy.asarray(center) - 0.5*size
        rtb_corner = numpy.asarray(center) + 0.5*size
        return cls(lbf_corner, rtb_corner)

    @classmethod
    def fromCoords(cls, coords):
        """Build bbox form points coordinates array N x dim
           If dim < 3 then bbox is padded with zeros.
        """
        lbf_corner = numpy.zeros((3,), dtype=numpy.float64)
        rtb_corner = numpy.zeros((3,), dtype=numpy.float64)
        lbf = coords.min(axis=0)
        rtb = coords.max(axis=0)
        lbf_corner[:lbf.size] = lbf
        rtb_corner[:rtb.size] = rtb
        return cls(lbf_corner, rtb_corner)


    def __getitem__(self, item):
        return self._corners[item, :]

    def __str__(self):
        return f"AABB: {self.lbf} {self.rtb}"

    @property
    def lbf(self):
        return self._corners[0, :]

    @property
    def rtb(self):
        return self._corners[1, :]

    @property
    def lbrt(self):
        return numpy.c[self._corners[0, 0:2], self._corners[1, 0:2]]

    @property
    def extents(self):
        return self._corners[1, :] - self._corners[0, :]

    @property
    def center(self):
        return numpy.mean(self._corners, axis=0)

    def scale(self, factor, inplace=True):
        """Get scaled box. Scale the box with box center being invariant point"""
        if isinstance(factor, float):
            factor = (factor,)*3
        factor = numpy.asarray(factor)-1.0
        ext = self.extents*factor*0.5
        if inplace:
            self._corners[0, :] -= ext
            self._corners[1, :] += ext
            return self
        else:
            return self.__class__(self._corners[0, :]-ext, self._corners[1, :]+ext)

    def offset(self, distance, inplace=True):
        """Move box corners symmetrically by given distance"""
        if isinstance(distance, float):
            distance = (distance,)*3
        offsets = numpy.asarray(distance)
        if numpy.any(self.extents+2*offsets < 0):
            idx = numpy.where(self.extents+2*offsets < 0.0)
            raise ValueError(f"Distance to big at %s" % (idx,))
        if inplace:
            self._corners[0, :] -= offsets
            self._corners[1, :] += offsets
            return self
        else:
            return self.__class__(self._corners[0, :]-offsets, self._corners[1, :]+offsets)

    def flatten(self, interleaved=True, dim=3):
        """If interleaved is False, the coordinates are in the
        form[xmin, xmax, ymin, ymax, ..., ..., kmin, kmax].If
        interleaved is True, the coordinates are int the
        form[xmin, ymin, ..., kmin, xmax, ymax, ..., kmax]"""
        if interleaved and dim == 3:
            return self._corners.flatten(order='C')
        elif interleaved and dim == 2:
            return self._corners[:, 0:2].flatten(order='C')
        elif not interleaved and dim == 3:
            return self._corners.flatten(order='F')
        elif not interleaved and dim == 2:
            return self._corners[:, 0:2].flatten(order='F')
        else:
            raise ValueError(f"Dimension {dim} not in (2,3)")

    def anchorAt(self, point, anchor='lbf'):
        anchor = anchor.lower()
        if anchor == 'lbf':
            tr = numpy.asarray(point) - self._corners[0]
        elif anchor == 'rtb':
            tr = numpy.asarray(point) - self._corners[1]
        elif anchor == 'center':
            tr = numpy.asarray(point) - self.center
        else:
            raise ValueError(f"Invalid anchor {anchor}. Expected one of 'lbf', 'rtb' or 'center'")
        self._corners[0, :] += tr
        self._corners[1, :] += tr

    def getFractionPoint(self, xfraction, yfraction, zfraction, scale=1.0):
        lbf = scale*(self.lbf - self.center)+self.center
        rtb = scale*(self.rtb - self.center)+self.center
        x = lbf[0]*(1-xfraction) + rtb[0]*xfraction
        y = lbf[1]*(1-yfraction) + rtb[1]*yfraction
        z = lbf[2]*(1-zfraction) + rtb[2]*zfraction
        return numpy.array([x, y, z])

    def getEdgeBox(self, location, eps=None):
        if eps is None:
            eps = max(1E-5, numpy.min(self.extents)/3)
        lbf = self.lbf
        rtb = self.rtb
        s, e = self.edges[location]
        lbf_mask = numpy.logical_not(self.points[s, :])
        lbf_mask_inv = self.points[s, :]
        rtb_mask = numpy.logical_not(self.points[e, :])
        rtb_mask_inv = self.points[e, :]
        eps_box_lbf = numpy.array([-eps, -eps, -eps])
        eps_box_rtb = numpy.array([eps, eps, eps])
        edge_lbf = eps_box_lbf + lbf * lbf_mask + rtb * lbf_mask_inv
        edge_rtb = eps_box_rtb + lbf * rtb_mask + rtb * rtb_mask_inv
        return self.__class__(edge_lbf, edge_rtb)

    def growByPoint(self, point):
        self._corners[0, :] = numpy.minimum(self.lbf, point)
        self._corners[1, :] = numpy.maximum(self.rtb, point)

    def growByAABB(self, aabb):
        self._corners[0, :] = numpy.minimum(self.lbf, aabb.lbf)
        self._corners[1, :] = numpy.maximum(self.rtb, aabb.rtb)
