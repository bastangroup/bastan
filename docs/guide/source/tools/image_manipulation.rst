.. include:: ../../replaces.txt
.. _image_manipulation-sec:

Image manipulation
==================


Krita
-----

.. figure:: /_static/images/krita_example.png
   :scale: 60 %

   Interface of Krita image manipulation program.
