"""Test solving buckling problems 
"""

__authors__ = 'RP'
__copyright__ = '(c) 2020 BASTAN Group'
__date__ = '2020.08.22  9:8:43'

import unittest
import json
import math
import numpy
from bastan import structures 
from bastan import jsonsupport
from bastan.fem import problems

class TestBuckling(unittest.TestCase):
    def test_truss_doom_buckling(self):
        fname = 'data/truss_doom.json'
        with open(fname) as fhandle:
            data = json.load(fhandle)
            params = data.get('params', dict())
            truss = structures.Truss.fromData(data, params=params)
            problem = problems.InitialBuckling()
            problem.solve(truss)
            critical = numpy.min(numpy.abs(numpy.real(problem.workspace.eigval)))
            print(f"Critical load: {critical}")
            print(numpy.sort(numpy.abs(numpy.real(problem.workspace.eigval))))

