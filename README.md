# BASTAN - BAr STructures ANalysis

BASTAN is Python backage for analysis of statics and stability of bars structures by Finite Element Method.

## BASTAN documentation

Sphinx generated user documentation is accessible at: [https://bastangroup.bitbucket.io/bastan](https://bastangroup.bitbucket.io/bastan)

## Requirements

Packages required to run BASTAN

  * simpleeval

## Configuration

The following environment variables need to be set

  * BASTAN_DIR -- path pointing to folder containing this file

## Authors

1. Pawel Bąk
2. Mateusz Orzeł
3. Roman Putanowicz

# References

