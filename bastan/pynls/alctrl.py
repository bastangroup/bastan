import numpy

from bastan.pynls import unisch
from bastan.pynls import ctrl
from bastan.pynls.ctrl import ControlType


class ArcLengthControl(unisch.UnifiedSchemes):
    def __init__(self, model, control, linsys):
        super().__init__(model, control, linsys)
        self._iDir = 1 
        self._dSumLambda = 0.0

        n = self.NumEq
        self._pdDx1_i1 = numpy.empty(n, dtype='float64')
        self._pdDx1_i_11 = numpy.empty(n, dtype='float64')

        self._pdSumDx = None
        if self.ctrlType == ctrl.ControlType.VARIABLE:
            self._pdSumDx = numpy.empty(n, dtype='float64')

    def Lambda(self):
        if self._iCurrIte == 1:
            numpy.copyto(self._pdDx1_i_11, self._pdDx1_i1)
    
            self._dLambda = numpy.sqrt(self._sControl.ctrlFactor**2.0 /
                    (1.0 + numpy.dot(self._pdDx1, self._pdDx1)))
            
            numpy.copyto(self._pdDx1_i1, self._pdDx1)
    
            if self._iCurrStep != 1:
                if numpy.dot(self._pdDx1_i_11, self._pdDx1_i1) < 0:
                    self._iDir *= -1
            self._dLambda *= self._iDir
            
            if self.ctrlType == ControlType.VARIABLE:
                self._dSumLambda = self._dLambda
                self._pdSumDx = self._dLambda * self._pdDx1
        else:    
            if self.ctrlType == ControlType.CONSTANT:
                self._dLambda = - numpy.dot(self._pdDx1_i1, self._pdDx2) / \
                        (numpy.dot(self._pdDx1_i1, self._pdDx1) + 1)
            else:
                self._dLambda = - numpy.dot(self._pdSumDx, self._pdDx2) / \
                        (numpy.dot(self._pdSumDx, self._pdDx1) + self._dSumLambda)
                self._dSumLambda += self._dLambda
                self._pdSumDx += self._dLambda*self._pdDx1 + self._pdDx2

# Almost agrees with Yang's Book...
#
# _sControl.CtrlFactor is DeltaS
#
# Neither matches the book exactly, the book calculates lambda_1 and
# DeltaU_1 and uses them, without updating, at every jth iteration
#
# VARIABLE - updates lambda and DeltaU at every jth iteration
# so lambda_1 is actually lambda_j-1 and DeltaU_1 is DeltaU_j-1
# _pdDx1 is DeltaUhat
# _pdDx2 is DeltaUbar
# _pdDxSum is DeltaU
#
# CONSTANT - sets lambda_1 = 1 and uses DeltaU_1 without updating
# _pdDx1 is DeltaUhat
# _pdDx2 is DeltaUbar
# _pdDx1_i1 is DeltaU
