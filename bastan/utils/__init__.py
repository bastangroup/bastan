"""
This package collect various modules that are used across whole BASTAN.
In particular it provides the following moules:

* ``enums`` - utilities for working with enumerations 
"""

if __debug__:
    print(f'Invoking __init__.py for {__name__}')


from . import enums
