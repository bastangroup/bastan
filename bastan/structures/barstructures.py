# -*- coding: utf-8 -*-
"""
This module provides definition to Truss class to model 3D trusses
"""

import numpy
import math

from bastan import jsonsupport
from bastan import bbox3D
from bastan.jsonsupport import JSONBased
from bastan import structures
from bastan import geom


class Bar(object):
    def __init__(self, structure, idx):
        self._structure = structure
        self._nodes = [] 
        self._angle = 0
        self._axes = numpy.zeros((3, 3), dtype='float64')
        self._sectionId = None
        self._idx = idx
        self._length = 0.0
        self._setup()

    def _setup(self):
        self._length = numpy.linalg.norm(self.startPoint - self.endPoint)
        self._axes[0, :] = (self.endPoint - self.startPoint)/self.length
        locY = numpy.cross([0, 0, 1], self._axes[0, :])
        locYnorm = numpy.linalg.norm(locY)
        if locYnorm < math.pi/360:
            self._axes[1, 0] = 1.0
        else:
            self._axes[1, :] = locY/locYnorm
        locZ = numpy.cross(self._axes[0, :], self._axes[1, :])
        locZnorm = numpy.linalg.norm(locZ)
        self._axes[2, :] = locZ/locZnorm

    def getAxis(self, i):
        return self._axes[i, :]

    @property
    def idx(self):
        return self._idx

    @property
    def nodes(self):
        return self._structure.elems[self.idx, :]

    @property
    def structure(self):
        return self._structure

    @property
    def startPoint(self):
        n = self.nodes[0]
        return self._structure.coords[n, :]

    @property
    def endPoint(self):
        n = self.nodes[1]
        return self._structure.coords[n, :]

    @property
    def center(self):
        return 0.5*(self.startPoint + self.endPoint)

    @property
    def axis(self):
        return self._axes[0, :]

    @property
    def axisX(self):
        return self._axes[0, :]

    @property
    def axisY(self):
        return self._axes[1, :]

    @property
    def axisZ(self):
        return self._axes[2, :]

    @property
    def length(self):
        return self._length

    @property
    def points(self):
        return self._structure.coords[self.nodes, :]

    @property
    def angle(self):
        return self._angle

    @property
    def exyz(self):
        return self.points.T.tolist() 

    @property
    def section(self):
        return self._structure.sections[self._sectionId]

    @property
    def orientation(self):
        return self._axes

class BarGeom(JSONBased):
    """Class that holds basic geometry and topology of a bar structure
    """
    JSONkey = 'bargeom'
    JSONattributes = [
            ("dim", jsonsupport.nochange),
            ("coords", jsonsupport.asparametricarray('float64')),
            ("elems", jsonsupport.asarray('uint'))
                ]

    def __init__(self):
        super().__init__()
        self.dim = 0
        self.coords = None
        self.elems = None
        self.sections = structures.Sections()
        self.bars = []
        self.coordsys = geom.CoordSys()

    def setup(self, data, params=None):
        super().setup(data, params=params)
        self.coordsys = geom.CoordSys.fromData(data)
        nelem = self.elems.size//2
        self.elems = self.elems.reshape((nelem, 2))
        npt = self.coords.size//3
        if npt*3 != self.coords.size:
            raise ValueError("Not enough coordinates (points are always 3D")
        self.coords = self.coords.reshape((npt, 3))
        self.coords = self.coordsys.toCartesian(self.coords, alwayscopy='False')
        for i in range(self.nbars):
            self.bars.append(Bar(self, i))
        self.sections.setup(data.get('sections'), params=params)
        self._resolve_sections()

    def _resolve_sections(self):
        for secname, sec in self.sections.items():
            for idx in sec.forBars:
                self.bars[idx]._sectionId = secname

    @property
    def nbars(self):
        """Number of bars"""
        return self.elems.shape[0]

    @property
    def nnodes(self):
        """Number of nodes"""
        return self.coords.shape[0]

    @property
    def is3D(self):
        """Return true if truss is 3D"""
        return self.coords.shape(1) == 3

    def getBBox(self):
        """Return bounding box of the structure"""
        return bbox3D.AABB.fromCoords(self.coords)


class Truss(JSONBased, structures.Structure):
    JSONkey = 'Truss'
    JSONattributes = {}

    def __init__(self):
        super().__init__()
        self.geom = BarGeom()
        self.supports = structures.Supports()
        self.loads = structures.Loads()
        self.materials = structures.Materials()

    def setup(self, data, params=None):
        super().setup(data, params=params)
        structures.Structure.setup(self, data)

    @property
    def ndofs_per_node(self):
        """Number of dofs per node"""
        return self.geom.coords.shape(1)


class Frame(JSONBased, structures.Structure):
    JSONkey = 'Frame'
    JSONattributes = {}

    def __init__(self):
        super().__init__()
        self.geom = BarGeom()
        self.supports = structures.Supports()
        self.loads = structures.Loads()
        self.materials = structures.Materials()

    def setup(self, data, params=None):
        super().setup(data, params=params)
        structures.Structure.setup(self, data)

    @property
    def ndofs_per_node(self):
        """Number of dofs per node"""
        return self.geom.coords.shape(1)
