bastan.examples package
=======================

.. automodule:: bastan.examples
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Submodules
----------

.. toctree::
   :maxdepth: 4

   bastan.examples._demo
