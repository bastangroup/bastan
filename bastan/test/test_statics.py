"""Test supports facilities 
"""

__authors__ = 'RP'
__copyright__ = '(c) 2020 BASTAN Group'
__date__ = '2020.08.04  9:8:43'
__version__ = '0.1.0'

import unittest
import json
import math
import numpy
from bastan import structures 
from bastan import jsonsupport
from bastan.fem import problems

DATA_STATICS="""
{
"Truss" : {
  "supports" : {
     "pin" : {
       "Tx" : "Fixed",
       "Ty" : 0.1,
       "atNodes" : [0,1]
     },
     "inplane" : {
       "Tz" : "Fixed",
       "atNodes" : ["0-2"]
     }
  },
  "bargeom" : {
    "dim" : 3,
    "coords" : [
       0.0, 0.0, 0.0,
       1.0, 1.0, 0.0,
       1.0, 0.0, 0.0],
    "elems" : [0,1,
            1,2],
    "sections" : {
      "column" : {
        "forBars" : ["0-1"],
        "area" : 0.1,
        "material" : "steelA"
      }
    }
  },
  "materials" : {
    "steelA" : {
      "E" : 2
    },
    "steelB" : {
      "E" : 4
    }
  },
  "loads" : {
    "cases" : {
      "LiveLoad_1" : {
        "nodal" : {
          "ForceA" : {
            "Fx" : 23.0,
            "atNodes" : [1]
          },
          "ForceB" : {
            "loads" : [140,11.0,0.0,"NoLoad","NoLoad","NoLoad"],
            "atNodes" : ["0-2"]
          }
        }
      },
      "LiveLoad_2" : {
        "nodal" : {
          "ForceA" : {
            "Fy" : 3.0,
            "atNodes" : [1]
          }
        }
      }
    }
  }
}
}
"""

class TestStatics(unittest.TestCase):
    def test_prepare_workspace(self):
        data = json.loads(DATA_STATICS)
        structure = structures.Truss.fromData(data)
        problem = problems.Statics()
        problem._prepare_workspace(structure)
        ws = problem.workspace
        self.assertEqual(ws.nnodes, 3)
        self.assertEqual(ws.ndofs, 9)
        self.assertEqual(ws.dofsPerNode, 3)
        self.assertEqual(ws.dofsPerElem, 6)
        self.assertEqual(ws.nelems, 2)
        self.assertTrue(ws.dofMap == {'Tx': 0, 'Ty': 1, 'Tz': 2})

    def test_supports_assembly(self):
        data = json.loads(DATA_STATICS)
        structure = structures.Truss.fromData(data)
        problem = problems.Statics()
        problem._prepare_workspace(structure)
        problem._assemble_supports(structure)
        ws = problem.workspace
        self.assertEqual(ws.bcDofs.size, 7)
        self.assertTrue(numpy.all(ws.bcDofs == [0, 1, 3, 4, 2, 5, 8]))
        self.assertTrue(numpy.all(ws.bcVals == [0.0, 0.1, 0.0, 0.1, 0.0, 0.0, 0.0]))

    def test_loads_assembly(self):
        data = json.loads(DATA_STATICS)
        structure = structures.Truss.fromData(data)
        problem = problems.Statics()
        problem._prepare_workspace(structure)
        problem._assemble_loads(structure, caseName='LiveLoad_1')
        ws = problem.workspace
        F = ws.get('F')
        self.assertEqual(F.size, 9)
        self.assertEqual(F.shape, (9,))
        self.assertAlmostEqual(F[0], 140.0)
        self.assertAlmostEqual(F[1], 11.0)
        self.assertAlmostEqual(F[2], 0.0)
        self.assertAlmostEqual(F[3], 163.0)
        self.assertAlmostEqual(F[4], 11.0)
        self.assertAlmostEqual(F[5], 0.0)
        self.assertAlmostEqual(F[6], 140.0)
        self.assertAlmostEqual(F[7], 11.0)
        self.assertAlmostEqual(F[8], 0.0)
    
    def test_load_case_assembly(self):
        data = json.loads(DATA_STATICS)
        structure = structures.Truss.fromData(data)
        problem = problems.Statics()
        problem._prepare_workspace(structure)
        problem._assemble_loads(structure, caseName="LiveLoad_2")
        F = problem.workspace.get('F')
        self.assertAlmostEqual(F[4], 3.0)
        self.assertAlmostEqual(F[0], 0.0)
        

    def test_assembly_linear_stiffness(self):
        data = json.loads(DATA_STATICS)
        structure = structures.Truss.fromData(data)
        problem = problems.Statics()
        problem._prepare_workspace(structure)
        problem._assemble_elements(structure)
        Kref = numpy.zeros((9,9), dtype='float64')
        K = problem.workspace.get('K')
        EAL1 = 0.2/math.sqrt(2) 
        cx=1/math.sqrt(2)
        cy = cx
        cz = 0
        K1 = EAL1*numpy.array([
            [cx*cx, cx*cy, cx*cz, -cx*cx, -cx*cy, -cx*cz],
            [cy*cx, cy*cy, cy*cz, -cy*cx, -cy*cy, -cy*cz],
            [cz*cx, cz*cy, cz*cz, -cz*cx, -cz*cy, -cz*cz],
            [-cx*cx, -cx*cy, -cx*cz, cx*cx, cx*cy, cx*cz],
            [-cy*cx, -cy*cy, -cy*cz, cy*cx, cy*cy, cy*cz],
            [-cz*cx, -cz*cy, -cz*cz, cz*cx, cz*cy, cz*cz]], dtype='float64')
        EAL2 = 0.2
        cx = 0
        cy = -1
        cz = 0
        K2 = EAL2*numpy.array([
            [cx*cx, cx*cy, cx*cz, -cx*cx, -cx*cy, -cx*cz],
            [cy*cx, cy*cy, cy*cz, -cy*cx, -cy*cy, -cy*cz],
            [cz*cx, cz*cy, cz*cz, -cz*cx, -cz*cy, -cz*cz],
            [-cx*cx, -cx*cy, -cx*cz, cx*cx, cx*cy, cx*cz],
            [-cy*cx, -cy*cy, -cy*cz, cy*cx, cy*cy, cy*cz],
            [-cz*cx, -cz*cy, -cz*cz, cz*cx, cz*cy, cz*cz]], dtype='float64')

        edof1 = [0, 1, 2, 3, 4, 5]
        edof2 = [3, 4, 5, 6, 7, 8]
        Kref[numpy.ix_(edof1, edof1)] += K1
        Kref[numpy.ix_(edof2, edof2)] += K2
        self.assertTrue(numpy.all(Kref == K))

    def test_calc_linear_stiffness(self):
        data = json.loads(DATA_STATICS)
        structure = structures.Truss.fromData(data)
        problem = problems.Statics()
        problem._prepare_workspace(structure)
        EAL = 0.2
        Kref = numpy.zeros((6, 6), dtype='float64')
        Kref[1, 1] = EAL
        Kref[1, 4] = -EAL
        Kref[4, 1] = -EAL
        Kref[4, 4] = EAL
        K = problem.fem.calcLinearStiffnessMatrix(structure, barIdx=1)
        self.assertTrue(numpy.all(Kref == K))

    def test_solving_misiak(self):
        fname = 'data/misiak_4_7.json'
        with open(fname) as fhandle:
            data = json.load(fhandle)
            params = data.get('params', dict())
            truss = structures.Truss.fromData(data, params=params)
            problem = problems.Statics()
            problem.solve(truss)
            self.assertEqual(truss.geom.nbars, 30)
            self.assertEqual(problem.workspace.nnodes, 12)
            self.assertEqual(problem.workspace.nelems, 30)
            self.assertEqual(problem.workspace.get('F').shape, (36,))
            P = jsonsupport.PARAMS['P']
            def getR(name, node):
                return problem.workspace.byDOF(name, atNode=node, within='R')
            places = 3
            self.assertAlmostEqual(getR("Tx", 8), -3.944305*P, places=places)
            self.assertAlmostEqual(getR("Ty", 8), -2*P, places=places)
            self.assertAlmostEqual(getR("Tz", 8), 0, places=places)
            self.assertAlmostEqual(getR("Tz", 10), 0, places=places)
            self.assertAlmostEqual(getR("Tx", 10), 2.055695*P, places=places)
            self.assertAlmostEqual(getR("Tx", 9), -2.055695*P, places=places)
            self.assertAlmostEqual(getR("Tx", 11), 3.944305*P, places=places)

    def test_solving_twobars_in_plane(self):
        fname = 'data/twobars_in_plane.json'
        with open(fname) as fhandle:
            data = json.load(fhandle)
            params = data.get('params', dict())
            truss = structures.Truss.fromData(data, params=params)
            problem = problems.Statics()
            problem.solve(truss)
            P = jsonsupport.PARAMS['P']
            def getR(name, node):
                return problem.workspace.byDOF(name, atNode=node, within='R')
            self.assertAlmostEqual(getR("Tz", 0), P)
            self.assertAlmostEqual(getR("Tz", 2), P)
            self.assertAlmostEqual(getR("Ty", 0), 0)
            self.assertAlmostEqual(getR("Ty", 1), 0)
            self.assertAlmostEqual(getR("Ty", 2), 0)
            self.assertAlmostEqual(getR("Tx", 0), 2*P)
            self.assertAlmostEqual(getR("Tx", 2), -2*P)

    def test_solving_threebars(self):
        fname = 'data/threebars.json'
        with open(fname) as fhandle:
            data = json.load(fhandle)
            params = data.get('params', dict())
            truss = structures.Truss.fromData(data, params=params)
            problem = problems.Statics()
            problem.solve(truss)
            with numpy.printoptions(precision=3, suppress=True):
                print(problem.workspace.get('F').reshape(4, 3))
            print('For threebars:')
            with numpy.printoptions(precision=3, suppress=True):
                print(problem.workspace.get('R').reshape(4, 3))
            P = jsonsupport.PARAMS['P']
            def getR(name, node):
                return problem.workspace.byDOF(name, atNode=node, within='R')
            self.assertAlmostEqual(getR("Tz", 0), P)
            self.assertAlmostEqual(getR("Tz", 1), P)
            Sx = getR("Tx", 0) + getR("Tx", 1) + getR("Tx", 2)
            Sy = getR("Ty", 0) + getR("Ty", 1) + getR("Ty", 2)
            Sz = getR("Tz", 0) + getR("Tz", 1) + getR("Tz", 2)
            self.assertAlmostEqual(Sx, 0.0)
            self.assertAlmostEqual(Sy, 0.0)
            self.assertAlmostEqual(Sz, 30.0)


    def test_solving_fourbars(self):
        fname = 'data/fourbars.json'
        with open(fname) as fhandle:
            data = json.load(fhandle)
            params = data.get('params', dict())
            truss = structures.Truss.fromData(data, params=params)
            problem = problems.Statics()
            problem.solve(truss)
            P = jsonsupport.PARAMS['P']
            def getR(name, node):
                return problem.workspace.byDOF(name, atNode=node, within='R')
            self.assertAlmostEqual(getR("Tx", 0), -2*P)
            self.assertAlmostEqual(getR("Ty", 0), 0.0)
            self.assertAlmostEqual(getR("Tz", 0), P)
            self.assertAlmostEqual(getR("Tx", 1), 0)
            self.assertAlmostEqual(getR("Ty", 1), -2*P)
            self.assertAlmostEqual(getR("Tz", 1), P)
