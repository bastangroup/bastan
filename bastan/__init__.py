import sys
import os

PKGDIR = os.path.dirname(__file__)
CALFEMDIR = os.path.join(PKGDIR, '..', 'external', 'pycalfem')

sys.path.append(CALFEMDIR)

from . import examples
from . import fem
from . import structures
