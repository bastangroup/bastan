import abc


class Model(abc.ABC):

    def __init__(self):
        self._iNumEpsEq = 0
        self._iNumEq = 0
        self._iNumGra = 0  # ? number of gradients
        self._outfile = None

    @property
    def NumEpsEq(self):
        return self._iNumEpsEq

    @property
    def NumEq(self):
        return self._iNumEq

    @abc.abstractmethod
    def Init(self):
        ...

    @abc.abstractmethod
    def InternalVector(self, u, f):
        ...

    @abc.abstractmethod
    def Reference(self, F):
        ...

    @abc.abstractmethod
    def ReportConvergence(self, dFactor, pdSol):
        ...

    @abc.abstractmethod
    def TangentMatrix(self, u, M):
        ...

    def Profile(self):
        """Return profile vector for the model"""
        pass  # FIXME

    def SparsityPattern(self):
        """Return sparisty matrix for the model"""
        pass  # FIXME

    def ReportConvergence (self, dFactor, dSolVec):
        """Write convergence to the output file"""
        pass  # FIXME

    def StrainVector(self):
        pass  # FIXME

    def DeltaStrainVector(self):
        pass  # FIXME
