# -*- coding: utf-8 -*-
"""Provide utilities to build demo programs.

Most notably it provides Demo class. To benefit from it one has to provide
demonstration code as demonstrate method of YourDemo class derived from Demo.

.. code-block:: python
    :caption Demo program template with use of `genfemcalc.examples.Demo` class

    from bastan import examples

    def MyDemo(examples.Demo):
        def demonstrate(self):
            self.report('This is my example executinon')

    if __name__ == '__main__':
        demo = MyDemo(__file__)
        demo.run()
"""

__authors__ = 'RP'
__copyright__ = '(c) 2020 BASTAN Group'
__date__ = 'Fri, Apr 17, 2020 12:12:47 AM'
__version__ = '0.1.0'

import abc
import argparse
import os
import timeit

import matplotlib.pyplot as plt

class Demo(abc.ABC):
    """Abstract base class for demos
    """

    @abc.abstractmethod
    def demonstrate(self, *args, **kwargs):
        """Override this method with function to execute your demo"""
        ...

    def __init__(self, name=None, silent=False):
        self.name = name
        if self.name is None:
            self.name = type(self).__name__
        self.silent = silent
        self._start_time = 0.0
        self._end_time = 0.0
        self._img_count = 0
        self.args = None
        self.show_images = True
        self.close_images = False
        self.save_images = True
        self.image_root = self.name
        self.image_format = 'png'
        self.outdir = '.'
        self.parser = argparse.ArgumentParser(description="Bastan Demo:")
        self._setup_parser()

    def _setup_parser(self):
        """Connfigure parser arguments
        """
        self.parser.add_argument('--verbose-gmsh', action='store_true', dest='verbose_gmsh',
                                 help='If True mesh generation is verbose')
        self.parser.add_argument('--close-images', action='store_true', dest='close_images',
                                 help='Automatically close image')
        self.parser.add_argument('--show-images', action='store_true', dest='show_images',
                                 help='Show images on screen')
        self.parser.add_argument('--save-images', action='store_true', dest='save_images',
                                 help='Save images on disk')
    def report(self, message):
        if not self.silent:
            print(message)

    def atStart(self):
        self._start_time = timeit.default_timer()
        self._end_time = self._start_time
        self.report('---------------------START DEMO------------------------')
        self.report(f"Running: {self.name}")

    def atEnd(self):
        self._end_time = timeit.default_timer()
        self.report('---------------------END of DEMO------------------------')
        self.printTiming()

    def printTiming(self):
        """Print information about demo execution time"""
        total_time = self._end_time - self._start_time
        self.report(f"{self.name} executed in {total_time:.{3}} seconds")

    def run(self):
        """Run demo code with some initialisation and finalization"""
        self.args,other = self.parser.parse_known_args()
        self.show_images = self.args.show_images
        self.close_images = self.args.close_images
        self.save_images = self.args.save_images
        self.atStart()
        self.demonstrate()
        self.atEnd()

    def saveImage(self, fig, add_counter=True, **kwargs):
        """Save figure as image file. The file name is taken form
        self.image_file member. The full path is set as:
        os.path.join(self.outdir, self.image_file)
        """
        if add_counter:
            fname = "%s_%03d.%s" % (self.image_root, self._img_count, self.image_format)
            self._img_count += 1
        else:
            fname = self.image_root + '.' + self.image_format
        fpath = os.path.join(self.outdir, fname)
        try:
            fig.savefig(fname=fpath, **kwargs)
        except IOError:
            print(f"WARNING: saving image as {fpath} FAILED")

    def showImage(self, pause=2):
        """Show image for givent time or indefinitely
        """
        if self.show_images:
            if self.close_images:
                plt.show(block=False)
                plt.pause(3)
                plt.close()
            else:
                plt.show()

