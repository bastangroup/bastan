# -*- coding: utf-8 -*-
"""
Module providing data structures describing loads
"""

import numpy
import math

from bastan import jsonsupport
from bastan.jsonsupport import JSONBased


class DistributedLoad(JSONBased):
    """Holds distributed(element) loads
    """
    LOADNAMES = ['Fx', 'Fy', 'Fz', 'Mx', 'My', 'Mz']
    KEYS = {'NoLoad': 0.0}
    JSONattributes = [
        ("atBars", jsonsupport.asranges)
    ]
    counter = 0
    @staticmethod
    def getName(name):
        if name is None:
            DistributedLoad.counter += 1
            name = f"DistributedLoad_{DistributedLoad.counter}"
        return name

    def __init__(self, name=None):
        self.components = {}
        self.atBars = []
        self.name = DistributedLoad.getName(name)

    def setup(self, data):
        jsonsupport.setParams(**self.KEYS)
        super().setup(data)
        for name in self.__class__.LOADNAMES:
            self.setupCompound(data, name, jsonsupport.PolVal, optional=True)


class NodalLoad(JSONBased):
    """Holds nodal load value
    """
    LOADNAMES = ['Fx', 'Fy', 'Fz', 'Mx', 'My', 'Mz']
    KEYS = {'NoLoad': 0.0}
    JSONattributes = [
            ("loads", jsonsupport.asparametricarray('float64')),
            ("atNodes", jsonsupport.asranges)
                ]
    counter = 0
    @staticmethod
    def getName(name):
        if name is None:
            NodalLoad.counter += 1
            name = f"NodalLoad_{NodalLoad.counter}"
        return name

    def __init__(self, name=None):
        self.loads = numpy.full((6,), 0.0, dtype='float64')
        self.atNodes = []
        self.name = NodalLoad.getName(name)

    def setup(self, data):
        jsonsupport.setParams(**NodalLoad.KEYS)
        super().setup(data)
        for idx, ln in enumerate(NodalLoad.LOADNAMES):
            if ln in data:
                self.loads[idx] = jsonsupport.fromparam(data[ln])
            

class LoadCase(JSONBased):
    JSONkey = 'LoadCase'
    counter = 0

    @staticmethod
    def getName(name):
        if name is None:
            LoadCase.counter += 1
            name = f"LoadCase_{counter}"
        return name

    def __init__(self, name):
        super().__init__()
        self.name = LoadCase.getName(name) 
        self.nodal = {}
        self.distributed = {}


    def setup(self, data):
        self.setupCompound(data, "nodal", NodalLoad, optional=True)
        self.setupCompound(data, "distributed", DistributedLoad, optional=True)


class Loads(JSONBased):
    JSONkey = 'loads'

    def __init__(self):
        self.cases = {}

    def setup(self, data):
        self.setupCompound(data, "cases", LoadCase)
