import math
import os
import re
import sys
import pandas
import argparse
import json
import copy
import time

import numpy

from bastan import jsonsupport
from bastan import structures
from bastan import fem

try:  
    os.environ["BASTAN_DIR"]
except KeyError: 
    print("Please set the environment variable BASTAN_DIR")
    sys.exit(1)

sys.path.append(os.environ['BASTAN_DIR'])


class ListProblemsAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=0, **kwargs):
        if nargs is not 0:
            raise ValueError("nargs not allowed")
        super().__init__(option_strings, dest, nargs=0, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        print('Available problems:')
        for problem in fem.Problem.derived():
            print(problem)
        sys.exit(0)


class Simulator(object):
    def __init__(self):
        self.data = None
        self.jsonData = None
        self.args = None
        self.parser = self.setupCommandLineParser()
        self.structure_file = None
        self.problem_file = None
        self.csv_file = None
        self.structure = None
        self.problem = None

    @staticmethod
    def setupCommandLineParser():
        parser = argparse.ArgumentParser(description='BArs STability ANalysis')
        parser.add_argument('-s','--structure', dest='structure_file', default='', help='JSON structure file')
        parser.add_argument('-p', '--problem', dest='problem_file', default='',
                            help='JSON structure file')
        parser.add_argument('--csv', dest='csv_file', default="",
                            help='CSV data file')
        parser.add_argument('--list-problems', metavar='', action=ListProblemsAction,
                            help='List available problems and exit')
        return parser

    @property
    def headers(self):
        """Headers of columns in CSV simulation file"""
        return self._headers

    def parseCommandLine(self):
        self.args = self.parser.parse_args()
        if not self.args.structure_file and self.args.csv_file:
            raise RuntimeError("CSV file given without JSON file")

    def haveInputFiles(self):
        """Return 0 if no input files were given, 1 if only JSON, 2 if JSON and CVS
        """
        n = 0
        if self.args.structure_file and self.args.csv_file:
            n = 2;
        elif self.args.structure_file:
            n = 1;
        return n
    
    def loadInputFiles(self):
        ns = 0
        if self.args.structure_file:
            self.readStructure(self.args.structure_file)
        if self.args.problem_file:
            self.readProblem(self.args.problem_file)
        if self.args.csv_file:
            ns = self.readCSV(self.args.csv_file)
        return ns

    def readStructure(self, path):
        self.structure_file = path
        with open(path) as fhandle:
            self.jsonData = json.load(fhandle, object_pairs_hook=jsonsupport.parse_object_pairs)
            jsonsupport.setupParams(self.jsonData)
            self.setupStructure(self.jsonData)

    def readProblem(self, path):
        pass

    def setupStructure(self, data):
        if "Truss" in data:
            self.structure = structures.Truss.fromData(data)
        elif "Frame" in data:
            self.structure = structures.Frame.fromData(data)
        else:
            raise RuntimError("No know structure type in input data")

    def readCSV(self, path):
        self.csv_file = path
        self.data = pandas.read_csv(path, sep=';', header=0)
        self._nSteps = len(self.data.index)
        self._headers = list(self.data)
        return self._nSteps

    def findBBox(self):
        return self.structure.geom.getBBox()

    def run(self):
        if self.problem is None:
            print('No problem specified ... do nothing')
        pass

if __name__ == "__main__":
    print("Starting application")
    simulator = Simulator()
    simulator.parseCommandLine()
    simulator.run()
