.. include:: ../replaces.txt
.. _tutorial-sec:

#########
Tutorials
#########

.. toctree::
   :maxdepth: 4

   basicpy/index
   calfem/index
   problems/index
