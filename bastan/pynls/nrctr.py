import numpy

from bastan.pynls import unisch
from bastan.pynls import ctrl
from bastan.pynls.ctrl import ControlType


class NewtonRaphsonControl(unisch.UnifiedSchemes):
    def __init__(self, model, control, linsys):
        super().__init__(model, control, linsys)

    def Lambda(self):
        if self._iCurrIte == 1:
            self._dLambda = self._sControl.ctrlFactor
        else:
            self._dLambda = 0.0
