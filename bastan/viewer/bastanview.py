import math
import os
import re
import sys
import pandas
import argparse
import json
import copy
import time
import logging

import matplotlib
import matplotlib.cm

import PySide2

from PySide2.QtWidgets import QApplication, QMainWindow, QFileDialog, QStatusBar
from PySide2.QtCore import QFile, QTimeLine, QEventLoop
from PySide2.Qt3DExtras import Qt3DExtras
from PySide2.Qt3DCore import Qt3DCore
from PySide2.QtGui import QVector3D, QColor, QVector4D, QQuaternion, QMatrix3x3
from PySide2 import QtGui
from PySide2 import QtCore
from PySide2.Qt3DRender import Qt3DRender
from PySide2.QtMultimedia import QMediaRecorder, QVideoEncoderSettings
from ui_mainwindow import Ui_MainWindow
from PySide2.QtCore import QObject, Signal, Slot

import numpy
import numpy as np

try:  
    os.environ["BASTAN_DIR"]
except KeyError: 
    print("Please set the environment variable BASTAN_DIR")
    sys.exit(1)

sys.path.append(os.environ['BASTAN_DIR'])

import bastan 
from bastan import simulator
from bastan.viewer import graphs
from bastan.viewer import draw3D
from bastan.viewer import glyphs
from bastan import bbox3D

from external.pyqtgraph.parametertree import ParameterTree, Parameter

# Uncomment below for terminal log messages
logger = logging.getLogger('showhum')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())

logger.info("All import OK")

PKGDIR = os.path.dirname(bastan.__file__)
DATADIR = os.path.join(PKGDIR, 'data')

def sliderFraction(slider):
    return slider.value()/float(slider.maximum())

def setSliderValue(slider, fraction):
    value = int(round((1-fraction)*slider.minimum() + fraction*slider.maximum()))
    return slider.setValue(value)

class MainWindow(QMainWindow):
    def __init__(self, simulator):
        super(MainWindow, self).__init__()
        self.glyphs = {}
        self.showFirstTime=True
        self.simulator = simulator
        self.temporalBBox = None
        self.userBBox = None
        self.all_temporalBBox = bbox3D.AABB()
        self.bbox = self.all_temporalBBox
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.createStatusBar()
        self.createGraphsPage()
        self.ui.pushButton.clicked.connect(self.echo)
        self.renderCapture = None

    def createGraphsPage(self):
        #self.graphCanvas = graphs.MplCanvas(self.ui.graphsFrame, width=5, height=4, dpi=100)
        #self.ui.graphsFrame.layout().addWidget(self.graphCanvas)
        self.graphsWidget = graphs.GraphsWidget(name='Multibody plot')
        self.ui.graphLayout.addWidget(self.graphsWidget)

        self.graphsParams = graphs.GraphsTree(self.simulator)
        params = [ {'name': 'Basic parameter data types', 'type': 'group'}]
        ## Create tree of Parameter objects
        p = Parameter.create(name='params', type='group', children=params)

        self.graphsParams.setGraphs(plotter=self.graphsWidget)
        self.ui.graphControlLayout.addWidget(self.graphsParams)

        #self.graphCanvas.axes.cla()  # Clear the canvas.
        #self.graphCanvas.axes.plot(x, y, '*-r')
        #self.graphCanvas.draw()

        #self.graphsSelection = QtGui.QStandardItemModel()
        #self.ui.graphsSelector.setModel(self.graphsSelection)
        #self.ui.graphsSelector.setRootIsDecorated(True)
        #self.ui.graphsSelector.setAlternatingRowColors(True)

        #parent = self.graphsSelection.invisibleRootItem()
        #for i in range(4):
        #   item = QtGui.QStandardItem(f"item {i}");
        #   item.setCheckState(QtCore.Qt.Unchecked)
        #   item.setCheckable(True)
        #   parent.appendRow(item)
        #   parent = item

    def updateGraphsParams(self):
        self.graphsParams.clear()
        self.graphsParams.setGraphs(plotter=self.graphsWidget)

    def createStatusBar(self):
        self.myStatus = QStatusBar()
        self.myStatus.showMessage("Status Bar Is Ready", 3000)
        self.setStatusBar(self.myStatus)

    def updateBBoxGlyph(self, bbox):
        extents = bbox.extents
        self.bboxMesh.setXExtent(extents[0])
        self.bboxMesh.setYExtent(extents[1])
        self.bboxMesh.setZExtent(extents[2])
        self.bboxTransform.setTranslation(QVector3D(*bbox.center.tolist()))

    def updateBBoxWidgets(self, bbox):
        self.ui.bboxLBF_X.setValue(bbox.lbf[0])
        self.ui.bboxLBF_Y.setValue(bbox.lbf[1])
        self.ui.bboxLBF_Z.setValue(bbox.lbf[2])
        self.ui.bboxRTB_X.setValue(bbox.rtb[0])
        self.ui.bboxRTB_Y.setValue(bbox.rtb[1])
        self.ui.bboxRTB_Z.setValue(bbox.rtb[2])

    def buildBBox(self, box_vertices):
        self.bboxMesh =  Qt3DExtras.QCuboidMesh()
        extents = box_vertices[1]-box_vertices[0]
        lbf = numpy.copy(box_vertices[0])
        rtb = numpy.copy(box_vertices[1])
        maxe = numpy.max(extents)
        for i in range(3):
            if extents[i] < 1e-5:
                extents[i] = maxe
                lbf[i] = box_vertices[0][i]-maxe/2
                rtb[i] = box_vertices[0][i]+maxe/2
        center = (rtb+lbf)/2
        pp = box_vertices[0]
        pp[2] += extents[2]
        self.camera.setPosition(QVector3D(*lbf.tolist()))
        self.camera.setViewCenter(QVector3D(center[0], center[1], center[2]))
        self.camera.setUpVector(QVector3D(0, 0, 1))

        self.bboxMesh.setXExtent(extents[0])
        self.bboxMesh.setYExtent(extents[1])
        self.bboxMesh.setZExtent(extents[2])
        self.bboxTransform = Qt3DCore.QTransform()
        self.bboxTransform.setTranslation(QVector3D(*center.tolist()))

        self.bboxMaterial = Qt3DExtras.QPhongAlphaMaterial()
        self.bboxMaterial.setDiffuse(QColor('Blue'))
        self.bboxMaterial.setAlpha(0.1)
        self.bboxEntity = Qt3DCore.QEntity(self.rootEntity)
        self.bboxEntity.addComponent(self.bboxMesh)
        self.bboxEntity.addComponent(self.bboxMaterial)
        self.bboxEntity.addComponent(self.bboxTransform)
        self.human_bbox = bbox3D.AABB(box_vertices[0], box_vertices[1])
        self.myStatus.showMessage(f"BBox: {box_vertices[0]} {box_vertices[1]}")

    def build_glyphs(self):
        self.glyphs.clear()
        for bar in self.simulator.structure.geom.bars:
            self.glyphs[f"bar_{bar.idx}"] = glyphs.BarGlyph(self.rootEntity,bar)

    def update_bbox(self):
        self.bbox = self.simulator.findBBox()
        self.updateBBoxWidgets(self.bbox)
        self.bboxChanged()


    def showEvent(self, event):
        super(MainWindow, self).showEvent(event)
        if self.showFirstTime:
            self.build3DView()
            self.ui.CameraController.updateCameraOnBBoxAlign()
            num_input = self.simulator.haveInputFiles()
            ns = self.simulator.loadInputFiles()
            if num_input > 0:
                self.updateOnJSONLoad()
            if num_input > 1:    
                self.updateOnCSVLoad(ns)
            self.showFirstTime=False

    def build3DView(self):
        self.view = Qt3DExtras.Qt3DWindow()
        self.container = self.createWindowContainer(self.view)
        self.container.setMinimumSize(800, 600)
        self.view.defaultFrameGraph().setClearColor('lightgrey')
        self.ui.viewlayout.addWidget(self.container)

        self.rootEntity = Qt3DCore.QEntity()
        self.camera = self.view.camera()
        videoSettings = QVideoEncoderSettings()
        videoSettings.setCodec("video/mpeg2")
        videoSettings.setResolution(640, 480)

        self.envLightEntity= Qt3DCore.QEntity(self.rootEntity)
        self.envlight = Qt3DRender.QEnvironmentLight(self.envLightEntity)
        self.envLightEntity.addComponent(self.envlight)

        #self.camController = Qt3DExtras.QFirstPersonCameraController(self.rootEntity)
        self.camController = Qt3DExtras.QOrbitCameraController(self.rootEntity)
        self.camController.setCamera(self.camera)
        self.view.setRootEntity(self.rootEntity)
        #self.view.defaultFrameGraph().setCamera(self.camera)
        self.camera.setParent( self.view.defaultFrameGraph().children()[0].children()[0].children()[0])
        #self.view.defaultFrameGraph().setCamera(self.camera)
        self.renderCapture = Qt3DRender.QRenderCapture(self.view.defaultFrameGraph().children()[0].children()[0].children()[0].children()[-1])
        #self.renderCapture = Qt3DRender.QRenderCapture(self.view.defaultFrameGraph().camera())
        self.globalCoordSys = draw3D.CoordSys(parent=self.rootEntity, lengths=(6.0, 6.0, 6.0))

        #renderSettings =  Qt3DRender.QRenderSettings()
        #renderStateSet = Qt3DRender.QRenderStateSet(self.view.defaultFrameGraph().children()[0].children()[0].children()[0].children()[-1])
        #renderStateSet = Qt3DRender.QRenderStateSet(self.view.defaultFrameGraph().children()[0].children()[0].children()[0].children()[0])
        #line_width = Qt3DRender.QLineWidth(self.view.defaultFrameGraph().children()[0].children()[0].children()[0].children()[0])
        #line_width = Qt3DRender.QLineWidth()
        #line_width.setValue(3.0)
        #renderStateSet.addRenderState(line_width)
        #self.rootEntity.addComponent(line_width)
        self.view.defaultFrameGraph().dumpObjectTree()
        self.ui.CameraController.setCamera(self.camera)
        self.ui.CameraController.setBBox(self.bbox)


    def updateOnCSVLoad(self, ns):
        self.setGlyphDataFromCSV()
        self.updateOnMaxStepsChanged(ns)
        self.all_temporalBBox = self.simulator.findBBox()
        self.temporalBBox = copy.deepcopy(self.all_temporalBBox)
        self.buildBBox(self.all_temporalBBox)
        self.updateGraphsParams()

    def updateOnJSONLoad(self):
        self.all_temporalBBox = self.simulator.findBBox()
        self.buildBBox(self.all_temporalBBox)
        self.build_glyphs()
        self.update_bbox()
        self.ui.CameraController.resetView()

    @Slot()
    def onPlayDuration(self, val):
        self.playDuration = int(1000*val)
        self.timeLine.setDuration(self.playDuration)

    @Slot()
    def onPlayStepForward(self, val):
        step = self.ui.stepSlider.value()
        if step < self.maxSteps:
            self.setStepSlider(step+1)

    @Slot()
    def onPlayStepBackward(self, val):
        step = self.ui.stepSlider.value()
        if step > 0:
            self.setStepSlider(step-1)

    @Slot()
    def onPlayGoStart(self, val):
        self.timeLine.stop()
        self.setStepSlider(0)

    @Slot()
    def onPlayGoEnd(self, val):
        self.timeLine.stop()
        self.setStepSlider(self.maxSteps)

    @Slot()
    def onPlayResume(self, val):
        self.timeLine.resume()

    @Slot()
    def onPlaySimulation(self, val):
        self.timeLine.setFrameRange(0, self.maxSteps)
        if val:
            self.timeLine.start()
        else:
            self.timeLine.stop()

    @Slot()
    def onBBoxRepNone(self, val):
        if val:
            self.bboxEntity.setEnabled(False)

    @Slot()
    def onBBoxRepSurface(self, val):
        if val:
            self.bboxEntity.setEnabled(True)

    @Slot()
    def onBBoxRepWireframe(self, val):
        pass

    @Slot()
    def onImageSave(self):
        filename = os.path.basename(self.simulator.json_file)
        base, ext = os.path.splitext(filename)
        step = self.getCurrentStep()
        path = f"{base}_{step:06}.png"
        self.reply.saveImage(path)
        self.myStatus.showMessage(f"Capture saved: {path}")

    @Slot()
    def onMovieCapture(self):
        startSteps = 0
        stopSteps = self.maxSteps
        for step in range(startSteps, stopSteps+1):
            event = QEventLoop()
            self.setStepSlider(step)
            self.onScreenshotCapture()
            self.reply.completed.connect(event.quit)
            event.exec_()
        self.myStatus.showMessage(f"Capture completed: captured {stopSteps-startSteps+1} frames")

    @Slot()
    def onScreenshotCapture(self):
        self.onBBoxRepNone(True)
        self.reply = self.renderCapture.requestCapture()
        #self.eventLoop = QEventLoop()
        self.reply.completed.connect(self.onImageSave)
        #while not self.reply.isCopleted():
        #    pritn('saving')
        #self.reply.image().save('ola.png')
        #print("Null image: ", self.reply.image().isNull())
        #self.eventLoop.exec_()
        #print("completed: ", self.reply.isComplete())

    @Slot()
    def echo(self):
        print('Reset view')
        self.ui.CameraController.resetView()

    @Slot()
    def onXDataChanged(self, value):
        self.graphsParams.onXDataChanged(value)

    def bboxChanged(self):
        self.updateBBoxGlyph(self.bbox)
        self.ui.CameraController.setBBox(self.bbox)

    @Slot()
    def onFileOpen(self):
        dialog = QFileDialog()
        filename = dialog.getOpenFileName(self, "Open")
        if len(filename[0]) > 0:
            self.simulator.readJSON(filename[0])
            self.updateOnJSONLoad()

    def updateOnMaxStepsChanged(self, ns):
        self.maxSteps = ns-1
        self.updateStepSlider(self.maxSteps)

    @Slot()
    def onLoadSimulationCSV(self):
        dialog = QFileDialog()
        filename = dialog.getOpenFileName(self, "Open")
        if len(filename[0]) > 0:
            ns = self.simulator.readCSV(filename[0])
            self.myStatus.showMessage(f"Number of steps: {ns}")
            self.updateOnCSVLoad(ns)


    def updateToStep(self, step):
        pass

    def updateGlyphsToStep(self, step):
        for name, glyph in self.glyphs.items():
            glyph.update(step)

    def getCurrentStep(self):
        return self.ui.stepSlider.value()

    @Slot()
    def onSetCurrentStep(self, step):
        self.updateGlyphsToStep(step)

    @Slot()
    def onQuit(self):
        PySide2.QtCore.QCoreApplication.instance().quit()

    @Slot()
    def onBBoxTypeChange(self):
        val = True
        if self.ui.userBBox.isChecked():
            val = True
            self.ui.temporalBBoxPane.setDisabled(True)
            self.ui.userBBoxPane.setEnabled(True)
            self.myStatus.showMessage("Use user bbox")
        elif self.ui.temporalBBox.isChecked():
            self.ui.temporalBBoxPane.setEnabled(True)
            self.ui.userBBoxPane.setDisabled(True)
            self.myStatus.showMessage("Use temporal bbox")
        self.bboxChanged()

    @Slot()
    def onStartStopTemporalBBStep(self, step):
        a = self.ui.startBBoxStep.value()
        b = self.ui.stopBBoxStep.value()
        startStep = numpy.min(a,b)
        stopStep = numpy.max(a,b)
        self.temporalBBox = simulator.findBBox(startStep, stopStep)
        self.bbox = self.temporalBBox
        self.bboxChanged()

    @Slot()
    def onCopyTemporalBBox(self):
        if self.ui.allStepsTemporalBBox.isChecked():
            self.userBBox = bbox3D.AABB(self.all_temporalBBox.lbf, self.all_temporalBBox.rtb)
        else:
            self.userBBox = bbox3D.AABB(self.temporalBBox.lbf, self.temporalBBox.rtb)
        self.ui.bboxLBF_X.setValue(self.userBBox.lbf[0])
        self.ui.bboxLBF_Y.setValue(self.userBBox.lbf[1])
        self.ui.bboxLBF_Z.setValue(self.userBBox.lbf[2])
        self.ui.bboxRTB_X.setValue(self.userBBox.rtb[0])
        self.ui.bboxRTB_Y.setValue(self.userBBox.rtb[1])
        self.ui.bboxRTB_Z.setValue(self.userBBox.rtb[2])

    @Slot()
    def onTemporalBBoxXYZ(self, val):
        pass

    @Slot()
    def onAllStepsBBox(self, value):
        pass

    def setGlyphDataFromCSV(self):
        for name, glyph in self.glyphs.items():
            orient = self.simulator.getColumns(name, 'OrientationMatrix_.*')
            glyph.setOrientation(orient.to_numpy(dtype='float64'))
            pos = self.simulator.getColumns(name, "R_.*")
            glyph.setLinearPosition(pos.to_numpy(dtype='float64'))

    def updateStepSlider(self, maxSteps):
        self.ui.stepSlider.setMaximum(maxSteps)
        self.ui.stepSpinbox.setMaximum(maxSteps)

    @Slot()
    def animate(self, _time):
        self.setStepSlider(self.timeLine.currentFrame())

    @Slot()
    def stop_animate(self):
        self.ui.playSimulation.setChecked(False)
        pass

    @Slot()
    def setStepSlider(self, step):
        self.ui.stepSlider.setValue(step)
        self.ui.stepSpinbox.setValue(step)

                    
if __name__ == "__main__":
    print("Starting application")
    app = QApplication(sys.argv)
    simulator = simulator.Simulator()
    simulator.parseCommandLine()
    window = MainWindow(simulator)
    window.show()
    sys.exit(app.exec_())
