"""
 This module contains the public data structure definitions for the control class.
"""

import abc
import copy
import numpy
import enum

from bastan import utils


class UpdateType(enum.IntEnum, metaclass=utils.enums.MetaIntEnum):
    """Enum update type
    """
    STANDARD = 0
    MODIFIED = 1


class ControlType(enum.IntEnum, metaclass=utils.enums.MetaIntEnum):
    CONSTANT = 0
    VARIABLE = 1


class ControlParams:
    def __init__(self):
        self.ctrlEq = 0  # int - current equation
        self.ctrlFactor = 0.0   # double - ???
        self.ctrlIniFactor = 0.0  # double - ???
        self.ctrlType = ControlType.CONSTANT
        self.numMaxIte = 0  # int -- ??? maximum number of iterations
        self.numMaxStep = 0  # int -- ??? maximum number of steps
        self.tol = 0.0  # tolerance
        self.updateType = UpdateType.STANDARD   


class Control(abc.ABC):
    def __init__(self, model, control, linsys):
        self._sControl = copy.deepcopy(control)
        self._pcModel = model
        self._pcLinSys = linsys

        self._iConvFlag = False
        self._iCurrIte = 0  # current iteration
        self._iCurrStep = 0  # current step
        self._dTotFactor = 0.0

        self._pdReference = numpy.zeros(model.NumEq)
        self._pcModel.Reference(self._pdReference)
        self._pcLinSys.Init(self._pcModel)

    @property
    def ctrlType(self):
        return self._sControl.ctrlType

    @property
    def NumEq(self):
        return self._pcModel.NumEq

    @abc.abstractmethod
    def Solver(self):
        ...
