"""Test JSON reading facilities
"""

__authors__ = 'RP'
__copyright__ = '(c) 2020 BASTAN Group'
__date__ = '2020.08.04  9:8:43'
__version__ = '0.1.0'

import unittest
import json
import numpy
from bastan import structures 
from bastan import jsonsupport

DATA_CASE_1 = """
{
"bargeom" : {
  "dim" : 3,
  "coords" : [
     0.0,0.0,0.0,
     "a",0.0,0.0,
     "a","2*a",0.0,
     1.0,"a",1.0],
  "elems" : [0,1,
            1,2,
            2,3],
  "sections" : {
    "column" : {
      "forBars" : [0,2],
      "A" : 0.1,
      "material" : "steelA"
    },
    "beam" : {
       "forBars" : [1],
       "A" : 0.2,
       "material" : "steelB"
    }
  }
},
"materials" : {
  "steelA" : {
    "E" : 2.0e4
  },
  "steelB" : {
    "E" : 4
  }
},
"supports" : {
   "suppA" : {
     "dofs" : [NaN, 12, NaN, NaN, NaN, 10, 10],
     "atNodes" : ["1-4",12]
   },
   "suppB" : {
     "dofs" : [0,0,0,0,0,0],
     "Ty" : "Free",
     "Ry" : 33.0,
     "Rz" : "Nan"
   }
},
"loads" : {
  "cases" : {
    "LiveLoad_1" : {
      "nodal" : {
        "ForceA" : {
          "Fx" : 23,
          "atNodes" : [1]
        },
        "ForceB" : {
          "loads" : [1,2,3,"NoLoad","NoLoad","NoLoad"],
          "atNodes" : ["1-4"]
        }
      }
    }
  }
}
}
"""

DATA_CASE_2 = """
{
"bargeom" : {
  "dim" : 3,
  "coords" : [
     0.0,0.0,0.0,
     "a",0.0,0.0,
     "a","2*a",0.0,
     1.0,"a",1.0],
  "elems" : [0,1,
            1,2,
            2,3],
  "sections" : {
    "column" : {
      "forBars" : [0,2],
      "area" : 0.1,
      "material" : "steelA"
    }
  }
  }
}
"""


class TestJSONRead(unittest.TestCase):
    def test_read_bargeom(self):
        fname = 'data/bargeom.json'
        with open(fname) as fhandle:
            data = json.load(fhandle)
            bargeom = structures.BarGeom.fromData(data)
            self.assertEqual(bargeom.dim, 3)
            self.assertEqual(bargeom.nnodes, 4)
            self.assertEqual(bargeom.nbars, 3)

    def test_read_parametric_bargeom(self):
        jsonsupport.setParams(a=23)
        data = json.loads(DATA_CASE_2)
        bargeom = structures.BarGeom.fromData(data)
        self.assertAlmostEqual(bargeom.coords[1, 0], 23.0)
        self.assertAlmostEqual(bargeom.coords[2, 1], 46.0)

    def test_read_truss(self):
        fname = 'data/truss.json'
        with open(fname) as fhandle:
            data = json.load(fhandle)
            truss = structures.Truss.fromData(data)
            geom = truss.geom
            self.assertEqual(geom.dim, 3)
            self.assertEqual(geom.nnodes, 4)
            self.assertEqual(geom.nbars, 3)

    def test_read_supports(self):
        data = json.loads(DATA_CASE_1)
        sc = structures.Supports.fromData(data)
        self.assertEqual(len(sc), 2)
        s1 = sc['suppA']
        self.assertTrue(numpy.isnan(s1.dofs[0]))
        self.assertAlmostEqual(s1.dofs[1], 12.0)
        self.assertListEqual(s1.atNodes.tolist(), [1, 2, 3, 4, 12])
        s2 = sc['suppB']
        self.assertTrue(numpy.isnan(s2.dofs[1]))
        self.assertTrue(numpy.isnan(s2.dofs[5]))
        self.assertAlmostEqual(s2.dofs[4], 33.0)

    def test_read_materials(self):
        data = json.loads(DATA_CASE_1)
        materials = structures.Materials.fromData(data)
        self.assertEqual(len(materials), 2)
        self.assertAlmostEqual(materials['steelA'].E, 20000.0)

    def test_read_loads(self):
        data = json.loads(DATA_CASE_1)
        loads = structures.Loads.fromData(data)
        self.assertEqual(len(loads.cases), 1)
        case = loads.cases['LiveLoad_1']
        self.assertEqual(len(case.nodal), 2)
        nl = case.nodal['ForceB']
        self.assertListEqual(nl.atNodes.tolist(), [1, 2, 3, 4])
        self.assertAlmostEqual(nl.loads[2], 3.0)
        self.assertAlmostEqual(nl.loads[3], 0.0)

    def test_read_polyval_twovalues(self):
        data1 = '''{"PolyVal": {"values" : [1, 3],
                    "ksi" : [0, 0.5]}}'''
        pv = jsonsupport.PolyVal.fromData(json.loads(data1))
        pv.setup(json.loads(data1))
        self.assertAlmostEqual(pv.ksi[1], 0.5)
        self.assertAlmostEqual(pv.values[1], 3.0)

    def test_read_polyval_onevalue(self):
        data1 = '''{"values" : [23.0],
                    "ksi" : [0, 0.5]}'''
        pv = jsonsupport.PolyVal.fromData(json.loads(data1), key="")
        self.assertAlmostEqual(pv.ksi[1], 0.5)
        self.assertAlmostEqual(pv.values[1], 23.0)
