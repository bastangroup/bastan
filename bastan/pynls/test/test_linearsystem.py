"""Test linear system solving utilities
"""

__authors__ = 'RP'
__copyright__ = '(c) 2020 BASTAN Group'
__date__ = 'Mon May  3 15:26:44 CEDT 2021'
__version__ = '0.1.0'

import unittest
from bastan import pynls
import numpy

class DummyModel:
    def __init__(self, N):
        self._numEq = N

    @property
    def numEq(self):
        return self._numEq

class TestLinSys(unittest.TestCase):
    def test_1D_solve (self):
        model = DummyModel(1)
        lsys = pynls.linearsystem.LinSys()
        lsys.Init(model)
        lsys.AddA(0,0,12);
        b = numpy.array([6.0])
        x = numpy.array([0.0])
        lsys.Solve(b, x)
        self.assertAlmostEqual(x[0],0.5,places=5)
