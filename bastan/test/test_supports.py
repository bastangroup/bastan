"""Test supports facilities 
"""

__authors__ = 'RP'
__copyright__ = '(c) 2020 BASTAN Group'
__date__ = '2020.08.04  9:8:43'
__version__ = '0.1.0'

import unittest
import json
import numpy
from bastan import structures 
from bastan import jsonsupport

DATA_CASE_1="""
{
"supports" : {
   "suppA" : {
     "dofs" : [NaN, 12, NaN, NaN, 10, 10],
     "atNodes" : ["1-4",12]
   },
   "suppB" : {
     "dofs" : [0,0,0,0,0,0],
     "Ty" : "Free",
     "Ry" : 33.0,
     "Rz" : "Nan"
   }
}
}
"""

class TestSupports(unittest.TestCase):
    def test_count_fixed_dofs(self):
        data = json.loads(DATA_CASE_1)
        sc = structures.Supports.fromData(data)
        s1 = sc['suppA']
        self.assertEqual(s1.countFixedDofs(), 3)
        s2 = sc['suppB']
        self.assertTrue(s2.countFixedDofs(['Tx', 'Ty']), 2)
        self.assertEqual(sc.countFixedDofs(['Tx', 'Ty', 'Rz']), 10)
