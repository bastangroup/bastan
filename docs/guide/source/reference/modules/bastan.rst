bastan package
==============

.. automodule:: bastan
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   bastan.examples
   bastan.fem
   bastan.structures
   bastan.test
   bastan.utils

Submodules
----------

.. toctree::
   :maxdepth: 4

   bastan.bbox3D
   bastan.geom
   bastan.jsonsupport
   bastan.simulator
