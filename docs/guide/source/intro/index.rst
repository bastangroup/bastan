.. include:: ../../replaces.txt
.. _intro-part:

############
Introduction
############

.. toctree::
   :maxdepth: 2

   intro.rst
   hands_on_example.rst
