# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import os
import sys
DOCTOPDIR=os.path.abspath(os.path.dirname(__name__))
#sys.path.append(os.path.join(os.path.dirname(__name__), '../..'))
#sys.path.append(od.path.abspaht(os.path.join(os.path.dirname(__name__), 'sphinx_packages/sphinxcontrib')))

sys.path.append(os.path.join(DOCTOPDIR, '../..'))
sys.path.append(os.path.join(DOCTOPDIR, 'sphinx_packages/sphinxcontrib'))

# -- Project information -----------------------------------------------------

project = 'bastan'
copyright = '2020, Jakub Pietrzak and Roman Putanowicz'
author = 'Jakub Pietrzak and Roman Putanowicz'

# The full version, including alpha/beta/rc tags
release = '1.0'

source_suffix = '.rst'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.doctest',
    'sphinx.ext.napoleon',
    'sphinx_gallery.gen_gallery',
    'tikz'
    ]

tikz_proc_suite='ImageMagick'

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

autodoc_member_order = 'groupwise'
autodoc_default_options = {
    'members': None,
    'member-order': 'groupwise',
    'special-members': '__init__',
    'undoc-members': True,
    'exclude-members': '__weakref__'
}
# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'alabaster'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# -- Options for HTML galery
sphinx_gallery_conf = {
    'filename_pattern': r'demo_',
    'ignore_pattern': r'__init__\.py',
    'examples_dirs': ['../../bastan/examples/basicpy',
                      '../../bastan/examples/calfem',
                      '../../bastan/examples/problems',
                      ],  # path to your example scripts
    'gallery_dirs': ['auto_examples/basicpy',
                     'auto_examples/calfem',
                     'auto_examples/problems',
                     ],  # path where to save gallery generated examples
}
# -- Options for LaTeX output --------------------------------------------------

latex_elements = {
    'preamble': r'\input{math_preamble.tex.txt}'
}

latex_additional_files = ["latex/math_preamble.tex.txt"]

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
latex_use_parts = True
latex_toplevel_sectioning = 'part'

