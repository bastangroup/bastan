.. include:: ../../replaces.txt
.. _intro-sec:

************
Introduction
************

BASTAN is an acronym from BAr STructures ANalysis. BASTAN is a package
for statics and stability analysis of bar structur by Finite Element Method.

Motivation
==========

Why to implement yet another FEM package if there are so many of them already
implemented? This is simple yet very important question because it touches the
issue of efficacy and role of research work, especially in academic context.

Package organisation
====================

BASTAN is divided into couple of smaller packages grouped more or less thematically
The are:

  * fem - package for calculations related to meshes and mesh elements. This inclueds
          interpolation in elements, calculation of forms in elements, integration of
          fields over meshes, etc.
  * examples - packages with demonstration code
  * test - packages for unit testing

On the directories level the structure of BASTAN is illustrated in Listing

.. code-block:: text
   :caption: Structure of BASTAN directories

bastan
 ├── bastan
 │   ├── examples
 │   │   ├── _demo.py
 │   │   ├── basicpy
 │   │   └── calfem
 │   ├── fem
 │   │   └── structural
 │   ├── structures
 │   └── test
 ├── buildserver
 ├── config
 ├── docs
 │   └── guide
 └── external

.. rubric:: Footnotes

.. [#nih] The example of cultural/psychological issue is NIH syndrome -
   Not Implemented/Invented Here.
