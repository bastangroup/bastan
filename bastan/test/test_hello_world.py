"""Just dumy unittest to demonstrate the testing facility
"""

__authors__ = 'RP'
__copyright__ = '(c) 2020 BASTAN Group'
__date__ = '2019.08.32  9:8:43'
__version__ = '0.1.0'

import unittest

class TestHelloWorld(unittest.TestCase):
    def setUp(self):
        self.ultimate_answer = 42 

    def test_ultimate_answer (self):
        self.assertTrue(self.ultimate_answer == 42, "Ultimate answer is OK")
